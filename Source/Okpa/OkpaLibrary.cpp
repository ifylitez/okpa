// OkpaLibrary.cpp: OkpaLibrary implementation.

#include "OkpaLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Engine/Engine.h"
#include "Sound/SoundBase.h"
#include "Classes/PaperSprite.h"
#include "Classes/PaperFlipbook.h"
#include "Classes/PaperFlipbookActor.h"
#include "Classes/PaperFlipbookComponent.h"
#include "Engine/StaticMesh.h"
#include "OkpaGameMode.h"
#include "OkpaHUD.h"


const TArray<FString> UOkpaLibrary::NamesOfEssentialPaperFlipbooks = {
	TEXT("BatFlyAnimation"),
	TEXT("DoveFlyAnimation"),
	TEXT("RavenFlyAnimation"),
	TEXT("RiverSplashAnimation")
};

const TArray<FString> UOkpaLibrary::NamesOfEssentialSoundWaves = {
	TEXT("ButtonClickSound"),
	TEXT("CatapultTriggerPullSound"),
	TEXT("CatapultTriggerUngripSound"),
	TEXT("FailureMusic"),
	TEXT("GameSessionMusic"),
	TEXT("MenuMusic"),
	TEXT("PopSound"),
	TEXT("RiverSplashSound"),
	TEXT("SuccessMusic"),
	TEXT("TargetHitSound"),
	TEXT("TargetMissSound"),
	TEXT("WheelbarrowArrivalSound")
};

const TArray<FString> UOkpaLibrary::NamesOfEssentialSprites = {
	TEXT("CatapultFrameSprite"),
	TEXT("CatapultPullerSprite"),
	TEXT("CatapultElasticBandSprite"),
	TEXT("StrongMagnetSprite"),
	TEXT("StrongProjectilePathSprite"),
	TEXT("WeakMagnetSprite"),
	TEXT("WeakProjectilePathSprite")
};

const TArray<FString> UOkpaLibrary::NamesOfEssentialStaticMeshes = {
	TEXT("BowlMesh"),
	TEXT("ChristmasTreeMesh"),
	TEXT("CoconutTreeMesh"),
	TEXT("MangoTreeMesh"),
	TEXT("OkpaPieceMesh"),
	TEXT("WoodMesh"),
	TEXT("RockMesh"),
	TEXT("TreeStumpMesh"),
	TEXT("WheelBarrowMesh")
};

const TArray<FString> UOkpaLibrary::NamesOfEssentialTextures = {
	TEXT("CalloutSmallBackgroundImage"),
	TEXT("FingerImage"),
	TEXT("VisualClueThatRepresentsClick"),
	TEXT("MagneticFieldEdgePointImage"),
	TEXT("MouseCursorImage"),
	TEXT("ProjectilePointImage")
};

const TArray<FString> UOkpaLibrary::NamesOfEssentialWidgetBlueprints = {
	TEXT("FailureScreen"),
	TEXT("GameSessionScreen"),
	TEXT("HomeScreen"),
	TEXT("MarketplaceScreen"),
	TEXT("MenuScreen"),
	TEXT("PauseScreen"),
	TEXT("StoryScreen"),
	TEXT("SuccessScreen")
};

const TArray<FDataAboutOkpaGameBooster> UOkpaLibrary::BoostersSoldInMarketplace = {
	{
		NAME_OF_WEAK_MAGNET,
		FText::FromString(TEXT("Attract okpas that are about 20 meters from the bowl.")),
		LoadObject<UPaperSprite>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("WeakMagnetSprite"))),
		30
	},
	{
		NAME_OF_STRONG_MAGNET,
		FText::FromString(TEXT("Attract okpas that are about 40 meters from the bowl.")),
		LoadObject<UPaperSprite>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("StrongMagnetSprite"))),
		80
	},
	{
		NAME_OF_WEAK_PROJECTILE_PATH,
		FText::FromString(TEXT("Elongate the projectile path to 80 meters.")),
		LoadObject<UPaperSprite>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("WeakProjectilePathSprite"))),
		20
	},
	{
		NAME_OF_STRONG_PROJECTILE_PATH,
		FText::FromString(TEXT("Elongate the projectile path to 120 meters.")),
		LoadObject<UPaperSprite>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("StrongProjectilePathSprite"))),
		70
	}
};


AOkpaGameMode* UOkpaLibrary::GetOkpaGameMode(const UObject* WorldContextObject)
{
	return Cast<AOkpaGameMode>(UGameplayStatics::GetGameMode(WorldContextObject));
}


AOkpaHUD* UOkpaLibrary::GetOkpaHUD(const UObject* WorldContextObject)
{
	return Cast<AOkpaHUD>(WorldContextObject->GetWorld()->GetFirstPlayerController()->GetHUD());
}


void UOkpaLibrary::QuitGameBecauseOfSomeReason(const FString& ReasonForQuittingGame, UObject* WorldContextObject)
{
	UKismetSystemLibrary::QuitGame(WorldContextObject, WorldContextObject->GetWorld()->GetFirstPlayerController(), EQuitPreference::Quit);
	UE_LOG(LogTemp, Error, TEXT("Game was quit because: %s"), *ReasonForQuittingGame);
}


bool UOkpaLibrary::SomeEssentialAssetsAreNotLoadable()
{
	return GetNamesOfEssentialAssetsThatAreNotLoadable().Len() != 0;
}


FString UOkpaLibrary::GetStringThatExplainsThatSomeEssentialAssetsAreNotLoadable()
{
	FString NamesOfEssentialAssetsThatAreNotLoadable = GetNamesOfEssentialAssetsThatAreNotLoadable();

	if (NamesOfEssentialAssetsThatAreNotLoadable.Len() == 0) {
		return FString();
	}
	else {
		return NamesOfEssentialAssetsThatAreNotLoadable.Append(TEXT(" could not be loaded."));
	}
}


FString UOkpaLibrary::GetNamesOfEssentialAssetsThatAreNotLoadable()
{
	FString UnloadableAssets;

	TArray<TArray<FString>> EssentialAssets = { NamesOfEssentialPaperFlipbooks, NamesOfEssentialSoundWaves, NamesOfEssentialSprites,
		NamesOfEssentialStaticMeshes, NamesOfEssentialTextures, NamesOfEssentialWidgetBlueprints };
	TArray<UClass*> ClassesOfEssentialAssets = { UPaperFlipbook::StaticClass(), USoundBase::StaticClass(), UPaperSprite::StaticClass(),
		UStaticMesh::StaticClass(), UTexture2D::StaticClass(), UUserWidget::StaticClass() };
	int Count = 0;

	for (int i = 0; i < EssentialAssets.Num(); ++i) {
		for (int j = 0; j < EssentialAssets[i].Num(); ++j) {
			if (!StaticLoadObject(ClassesOfEssentialAssets[i], nullptr, *GetReferencePathOfAsset(EssentialAssets[i][j])) && !StaticLoadClass(ClassesOfEssentialAssets[i], nullptr, *GetReferencePathOfAsset(EssentialAssets[i][j]))) {
				UnloadableAssets.Append(Count > 0 ? TEXT(" and ") : TEXT("")).Append(EssentialAssets[i][j]);
				++Count;
			}
		}
	}

	return UnloadableAssets;
}


FString UOkpaLibrary::GetReferencePathOfAsset(const FString& NameOfAsset)
{
	if (NamesOfEssentialPaperFlipbooks.Find(NameOfAsset) != INDEX_NONE) {
		return FString(TEXT("PaperFlipbook'")).Append(FOLDER_WHERE_ESSENTIAL_PAPER_FLIPBOOKS_ARE_STORED).Append(NameOfAsset).Append(TEXT(".")).Append(NameOfAsset).Append(TEXT("'"));
	}
	else if (NamesOfEssentialSoundWaves.Find(NameOfAsset) != INDEX_NONE) {
		return FString(TEXT("SoundWave'")).Append(FOLDER_WHERE_ESSENTIAL_SOUND_WAVES_ARE_STORED).Append(NameOfAsset).Append(TEXT(".")).Append(NameOfAsset).Append(TEXT("'"));
	}
	else if (NamesOfEssentialSprites.Find(NameOfAsset) != INDEX_NONE) {
		return FString(TEXT("PaperSprite'")).Append(FOLDER_WHERE_ESSENTIAL_SPRITES_ARE_STORED).Append(NameOfAsset).Append(TEXT(".")).Append(NameOfAsset).Append(TEXT("'"));
	}
	else if (NamesOfEssentialStaticMeshes.Find(NameOfAsset) != INDEX_NONE) {
		return FString(TEXT("StaticMesh'")).Append(FOLDER_WHERE_ESSENTIAL_STATIC_MESHES_ARE_STORED).Append(NameOfAsset).Append(TEXT(".")).Append(NameOfAsset).Append(TEXT("'"));
	}
	else if (NamesOfEssentialTextures.Find(NameOfAsset) != INDEX_NONE) {
		return FString(TEXT("Texture2D'")).Append(FOLDER_WHERE_ESSENTIAL_TEXTURES_ARE_STORED).Append(NameOfAsset).Append(TEXT(".")).Append(NameOfAsset).Append(TEXT("'"));
	}
	else if (NamesOfEssentialWidgetBlueprints.Find(NameOfAsset) != INDEX_NONE) {
		return FString(FOLDER_WHERE_ESSENTIAL_WIDGET_BLUEPRINTS_ARE_STORED).Append(NameOfAsset).Append(TEXT(".")).Append(NameOfAsset).Append(TEXT("_C"));
	}
	else {
		return FString();
	}
}


FVector UOkpaLibrary::GetSizeOfTypicalOkpaPiece()
{
	UStaticMesh* OkpaPieceMesh = LoadObject<UStaticMesh>(nullptr, *GetReferencePathOfAsset(TEXT("OkpaPieceMesh")));

	if (OkpaPieceMesh == nullptr) {
		return FVector::ZeroVector;
	}
	else {
		return OkpaPieceMesh->GetBoundingBox().GetSize();
	}
}


FVector UOkpaLibrary::GetWorldLocationWhereTouchInputOrMouseInputOccurred(const UObject* WorldContextObject)
{
	if (WorldContextObject == nullptr || WorldContextObject->GetWorld() == nullptr) {
		return FVector::ZeroVector;
	}

	FVector2D ScreenLocation;
	FVector WorldLocation, WorldDirection;
	bool TouchStatus = false, MouseStatus = false;

	WorldContextObject->GetWorld()->GetFirstPlayerController()->GetInputTouchState(ETouchIndex::Touch1, ScreenLocation.X, ScreenLocation.Y, TouchStatus);

	if (TouchStatus == false) {
		MouseStatus = WorldContextObject->GetWorld()->GetFirstPlayerController()->GetMousePosition(ScreenLocation.X, ScreenLocation.Y);
	}

	if (TouchStatus == false && MouseStatus == false) {
		return FVector::ZeroVector;
	}

	WorldContextObject->GetWorld()->GetFirstPlayerController()->DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y, WorldLocation, WorldDirection);
	return LOCATION_OF_CAMERA + (WorldDirection * FMath::Abs((Y_LOCATION_OF_CATAPULT - Y_LOCATION_OF_CAMERA) / WorldDirection.Y));
}


void UOkpaLibrary::DestroyAllActorsOfClass(TSubclassOf<AActor> Class, const UObject* WorldContextObject)
{
	TArray<AActor*> AllActorsOfClass;

	UGameplayStatics::GetAllActorsOfClass(WorldContextObject, Class, AllActorsOfClass);

	for (AActor* Actor : AllActorsOfClass) {
		Actor->Destroy();
	}
}


FString UOkpaLibrary::BreakIntoSeveralLines(FString StringBeingBroken, int MaximumCountOfCharactersForOneLine, int MaximumCountOfWordsForOneLine)
{
	int IndexOfFirstCharacterOfCurrentWord = 0;
	int CountOfCharactersInCurrentLine = 0;
	int CountOfWordsInCurrentLine = 0;

	StringBeingBroken.Replace(TEXT("\n"), TEXT(" "));

	for (int IndexOfCurrentCharacter = 0; IndexOfCurrentCharacter < StringBeingBroken.Len(); ++IndexOfCurrentCharacter) {
		++CountOfCharactersInCurrentLine;

		if (IndexOfCurrentCharacter == 0) {
			if (!isspace(StringBeingBroken[IndexOfCurrentCharacter])) {
				++CountOfWordsInCurrentLine;
			}
		}
		else {
			if (!isspace(StringBeingBroken[IndexOfCurrentCharacter]) && isspace(StringBeingBroken[IndexOfCurrentCharacter - 1])) {
				IndexOfFirstCharacterOfCurrentWord = IndexOfCurrentCharacter;
				++CountOfWordsInCurrentLine;
			}
		}

		if (CountOfCharactersInCurrentLine >= MaximumCountOfCharactersForOneLine && CountOfWordsInCurrentLine > 1) {
			IndexOfCurrentCharacter = IndexOfFirstCharacterOfCurrentWord - 1;
			StringBeingBroken[IndexOfCurrentCharacter] = TEXT('\n');
			CountOfCharactersInCurrentLine = 0;
			CountOfWordsInCurrentLine = 0;
		}

		if (isspace(StringBeingBroken[IndexOfCurrentCharacter]) && CountOfWordsInCurrentLine >= MaximumCountOfWordsForOneLine) {
			StringBeingBroken[IndexOfCurrentCharacter] = TEXT('\n');
			CountOfCharactersInCurrentLine = 0;
			CountOfWordsInCurrentLine = 0;
		}
	}

	return StringBeingBroken;
}


void UOkpaLibrary::PlayAnimationAtLocation(const UObject* WorldContextObject, UPaperFlipbook* Animation, FVector Location)
{
	APaperFlipbookActor* NewPaperFlipbookActor = WorldContextObject->GetWorld()->SpawnActor<APaperFlipbookActor>(Location, FRotator::ZeroRotator);

	if (NewPaperFlipbookActor && NewPaperFlipbookActor->GetRenderComponent()) {
		NewPaperFlipbookActor->GetRenderComponent()->SetFlipbook(Animation);
		NewPaperFlipbookActor->GetRenderComponent()->SetLooping(false);
		NewPaperFlipbookActor->GetRenderComponent()->PlayFromStart();
		NewPaperFlipbookActor->GetRenderComponent()->OnFinishedPlaying.AddDynamic(GetOkpaGameMode(WorldContextObject), &AOkpaGameMode::ProcessPaperFlipbookAnimationFinish);
	}
}


FDataAboutOkpaGameBooster UOkpaLibrary::GetDataAboutBooster(const FName& NameOfBooster)
{
	for (int i = 0; i < BoostersSoldInMarketplace.Num(); ++i) {
		if (BoostersSoldInMarketplace[i].Name == NameOfBooster) {
			return BoostersSoldInMarketplace[i];
		}
	}

	return FDataAboutOkpaGameBooster();
}


TArray<FDataAboutOkpaGameBooster> UOkpaLibrary::GetDataAboutBoostersSoldInMarketplace()
{
	return BoostersSoldInMarketplace;
}


bool UOkpaLibrary::IsValidNameOfBooster(const FName& InName)
{
	for (int i = 0; i < BoostersSoldInMarketplace.Num(); ++i) {
		if (BoostersSoldInMarketplace[i].Name == InName) {
			return true;
		}
	}

	return false;
}
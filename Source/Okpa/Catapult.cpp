// Catapult.cpp: Catapult class implementation.

#include "Catapult.h"
#include "UObject/ConstructorHelpers.h"
#include "Classes/PaperSprite.h"
#include "Kismet/GameplayStatics.h"
#include "OkpaLibrary.h"
#include "OkpaHUD.h"
#include "CatapultTrigger.h"

#define RELATIVE_BASE_LOCATION_OF_TRIGGER FVector(0, 0, 200)


ACatapult::ACatapult()
{
	static ConstructorHelpers::FObjectFinder<UPaperSprite> ObjectFinder(*UOkpaLibrary::GetReferencePathOfAsset(TEXT("CatapultFrameSprite")));
	RenderComponent = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("RenderComponent"));

	if (RenderComponent != nullptr) {
		RenderComponent->SetSprite(ObjectFinder.Object);

		if (RootComponent != nullptr) {
			RootComponent->DestroyComponent();
		}

		RootComponent = RenderComponent;
	}
}


void ACatapult::BeginPlay()
{
	Super::BeginPlay();
	Activate();
}


void ACatapult::Activate()
{
	DeactivateAllExistingCatapults(this);

	Trigger = Cast<ACatapultTrigger>(UGameplayStatics::GetPlayerPawn(this, 0));

	if (Trigger != nullptr) {
		Trigger->SetActorLocation(GetBaseLocationOfTrigger());
		Trigger->SetActorHiddenInGame(false);
		Trigger->SetParentCatapult(this);
	}
}


void ACatapult::DeactivateAllExistingCatapults(const UObject* WorldContextObject)
{
	TArray<AActor*> AllExistingCatapults;

	UGameplayStatics::GetAllActorsOfClass(WorldContextObject, ACatapult::StaticClass(), AllExistingCatapults);

	for (AActor* Catapult : AllExistingCatapults) {
		Cast<ACatapult>(Catapult)->Deactivate();
	}
}


void ACatapult::Deactivate()
{
	if (IsActivated()) {
		Trigger->Reinitialize();
		Trigger = nullptr;
	}
}


FVector ACatapult::GetBaseLocationOfTrigger() const
{
	return this->GetActorLocation() + RELATIVE_BASE_LOCATION_OF_TRIGGER;
}


FVector ACatapult::GetCurrentLocationOfTrigger() const
{
	if (IsActivated()) {
		return Trigger->GetActorLocation();
	}
	else {
		return GetBaseLocationOfTrigger();
	}
}


bool ACatapult::IsActivated() const
{
	return Trigger != nullptr;
}


void ACatapult::Tick(float DeltaTime)
{
	bool IsMovingAtStartOfThisTick = IsMoving();

	Super::Tick(DeltaTime);

	if (IsMovingAtStartOfThisTick && IsActivated()) {
		Trigger->SetActorLocation(GetBaseLocationOfTrigger());
	}
}


bool ACatapult::SetActorLocation(const FVector& NewLocation, bool bSweep, FHitResult* OutSweepHitResult, ETeleportType Teleport)
{
	bool Status = Super::SetActorLocation(NewLocation, bSweep, OutSweepHitResult, Teleport);
	
	if (IsActivated()) {
		Trigger->SetActorLocation(GetBaseLocationOfTrigger(), bSweep);
	}

	return Status;
}


void ACatapult::DeleteProjectilePathOfTrigger()
{
	if (IsActivated()) {
		Trigger->DeleteProjectilePath();
	}
}


void ACatapult::SetLengthOfProjectilePathOfTrigger(float Length)
{
	if (IsActivated()) {
		Trigger->SetLengthOfProjectilePath(Length);
	}
}


bool ACatapult::TriggerIsNotEmpty() const
{
	return IsActivated() && Trigger->IsNotEmpty();
}


bool ACatapult::TriggerIsHoldingOkpaPiece(const AOkpaPiece* OkpaPiece) const
{
	return IsActivated() && Trigger->IsHoldingOkpaPiece(OkpaPiece);
}


void ACatapult::CauseTriggerToBecomeUngriped()
{
	if (IsActivated()) {
		Trigger->Ungrip();
	}
}


void ACatapult::CauseTriggerToHoldOkpaPiece(AOkpaPiece* OkpaPiece)
{
	if (IsActivated()) {
		Trigger->HoldOkpaPiece(OkpaPiece);
	}
}


void ACatapult::CauseTriggerToStopHoldingHeldOkpaPiece()
{
	if (IsActivated()) {
		Trigger->StopHoldingHeldOkpaPiece();
	}
}


USceneComponent* ACatapult::GetRenderComponent() const
{
	return RenderComponent;
}
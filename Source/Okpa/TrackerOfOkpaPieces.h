// TrackerOfOkpaPieces.h: TrackerOfOkpaPieces class definition.

#pragma once

#include "CoreMinimal.h"
#include "OkpaPiece.h"
#include "TrackerOfOkpaPieces.generated.h"


/**
 * TrackerOfOkpaPieces defines an object that is used by the OkpaGameMode to keep track of every okpa piece in this game's world.
 * @author Ejiofor Ifechukwu.
 */
UCLASS()
class OKPA_API UTrackerOfOkpaPieces : public UObject
{
	GENERATED_BODY()

public:
	/** Keeps track of the specified okpa piece as inside a wheelbarrow. */
	void TrackAsInsideWheelbarrow(AOkpaPiece* OkpaPieceThatShouldBeTracked);

	/** Keeps track of the specified okpa piece as inside a catapult's trigger */
	void TrackAsInsideCatapultTrigger(AOkpaPiece* OkpaPieceThatShouldBeTracked);

	/** Keeps track of the specified okpa piece as lauched */
	void TrackAsLaunched(AOkpaPiece* OkpaPieceThatShouldBeTracked);

	/** Keeps track of the specified okpa piece as inside a bowl. */
	void TrackAsInsideBowl(AOkpaPiece* OkpaPieceThatShouldBeTracked);

	/** Keeps track of the specified okpa piece as littered. */
	void TrackAsLittered(AOkpaPiece* OkpaPieceThatShouldBeTracked);

	/** Keeps track that, although the specified okpa piece is littered, it has not yet fully stopped moving. */
	void TrackAsLitteredButNotFullyStopped(AOkpaPiece* OkpaPieceThatShouldBeTracked);

	/** Keeps track that the specified okpa piece is both littered and has fully stopped moving. */
	void TrackAsLitteredAndFullyStopped(AOkpaPiece* OkpaPieceThatShouldBeTracked);

	/** Stops keeping track of the specified okpa piece if the specified okpa piece is inside a bowl. */
	void StopTrackingFromInsideBowl(AOkpaPiece* OkpaPieceThatShouldBeStopTrackinged);

	/** Stops keeping track of all okpa pieces. */
	void StopTrackingAllTrackedOkpaPieces();

	/** Returns the okpa pieces that are littered but not fully stopped moving. */
	TArray<AOkpaPiece*> GetOkpaPiecesThatAreLitteredButNotFullyStopped() const;

	/** Returns true if the specified okpa piece is inside a bowl. Returns false otherwise. */
	bool OkpaPieceIsInsideBowl(AOkpaPiece* OkpaPiece) const;

	/** Returns true if the specified okpa piece is inside a catapult trigger. Returns false otherwise. */
	bool OkpaPieceIsInsideCatapultTrigger(AOkpaPiece* OkpaPiece) const;

	/** Returns true if the specified okpa piece is inside a wheelbarrow. Returns false otherwise. */
	bool OkpaPieceIsInsideWheelbarrow(AOkpaPiece* OkpaPiece) const;

	/** Returns true if the specified okpa piece is launched. Returns false otherwise. */
	bool OkpaPieceIsLaunched(AOkpaPiece* OkpaPiece) const;

	/** Returns true if the specified okpa piece is littered. Returns false otherwise. */
	bool OkpaPieceIsLittered(AOkpaPiece* OkpaPiece) const;

	/** Returns true if all okpa pieces have landed. Returns false otherwise.
		@note An okpa piece has landed if it has either littered or entered inside bowl. */
	bool AllOkpaPiecesHaveLanded() const;

private:
	/** Stops keeping track of information about the specified okpa pieces. */
	void StopTracking(AOkpaPiece* OkpaPieceThatShouldBeStopTrackinged);

private:
	/** Okpa pieces that are inside a wheelbarrow. */
	TArray<AOkpaPiece*> OkpaPiecesThatAreInsideWheelbarrow;

	/** Okpa pieces that are inside a catapult's trigger. */
	TArray<AOkpaPiece*> OkpaPieceThatAreInsideCatapultTrigger;

	/** Okpa pieces that are launched. */
	TArray<AOkpaPiece*> OkpaPieceThatAreLaunched;

	/** Okpa pieces that are inside a bowl. */
	TArray<AOkpaPiece*> OkpaPiecesThatAreInsideBowl;

	/** Okpa pieces are littered but have never fully stopped. */
	TArray<AOkpaPiece*> OkpaPiecesThatAreLitteredButNotFullyStopped;

	/** Okpa pieces are littered and fully stopped. */
	TArray<AOkpaPiece*> OkpaPiecesThatAreLitteredAndFullyStopped;
};
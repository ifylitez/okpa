// FadeoutText.cpp: FadeoutText class implementation.

#include "FadeoutText.h"
#include "Components/TextRenderComponent.h"

#define DEFAULT_LIFESPAN			2.0
#define DEFAULT_EARLY_SCALE_SPEED	100.0


AFadeoutText::AFadeoutText()
	: Age(0.0), EarlyScaleSpeed(DEFAULT_EARLY_SCALE_SPEED), LateScaleSpeed(0.0)
{
	PrimaryActorTick.bCanEverTick = true;
}


void AFadeoutText::BeginPlay()
{
	Super::BeginPlay();
	SetLifeSpan(DEFAULT_LIFESPAN);
}


void AFadeoutText::SetLifeSpan(float NewLifespan)
{
	Super::SetLifeSpan(NewLifespan);
	ReCalculateLateScaleSpeed();
}


void AFadeoutText::SetScaleSpeed(float NewScaleSpeed)
{
	EarlyScaleSpeed = NewScaleSpeed;
	ReCalculateLateScaleSpeed();
}


void AFadeoutText::ReCalculateLateScaleSpeed()
{
	if (InitialLifeSpan == 0) {
		LateScaleSpeed = 0;
	}
	else {
		LateScaleSpeed = ((-3 * GetTextRender()->WorldSize) - (EarlyScaleSpeed * InitialLifeSpan)) / (2 * InitialLifeSpan);
	}
}


void AFadeoutText::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Age += DeltaTime;
	float DeltaScale = (Age < InitialLifeSpan / 3) ? (EarlyScaleSpeed * DeltaTime) : (LateScaleSpeed * DeltaTime);
	GetTextRender()->SetWorldSize(GetTextRender()->WorldSize + DeltaScale);
}
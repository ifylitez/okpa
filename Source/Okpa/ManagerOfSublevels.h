// ManagerOfSublevels.h: ManagerOfSublevels class definition.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraActor.h"
#include "OkpaLibrary.h"
#include "ManagerOfSublevels.generated.h"


/**
 * OkpaSublevelIdentifier defines constants that are used to identify the first ten sublevels of the Okpa game.
 * @note	In the Okpa game, the difference between the numeric values of any two consecutive sublevel identifiers is always one.
 *			For example, if the tenth sublevel of the Okpa game (defined by OkpaSublevelIdentifier as TENTH_SUBLEVEL) is equal to a numeric value of 10, then the eleventh sublevel (although undefined by OkpaSublevelIdentifier) will be equal to a numeric value of 11.
 * @author	EjioforIfechukwu.
 */
UENUM()
enum EOkpaSublevelIdentifier {
	FIRST_SUBLEVEL = 1,
	SECOND_SUBLEVEL,
	THIRD_SUBLEVEL,
	FOURTH_SUBLEVEL,
	FIFTH_SUBLEVEL,
	SIXTH_SUBLEVEL,
	SEVENTH_SUBLEVEL,
	EIGHTH_SUBLEVEL,
	NINETH_SUBLEVEL,
	TENTH_SUBLEVEL
};


/**
 * DataAboutSpawnedMovableObstacle defines the data that is stored, by ManagerOfSublevels, about every movable obstacle that it spawns.
 * @author EjioforIfechukwu.
 */
USTRUCT()
struct FDataAboutSpawnedMovableObstacle
{
	GENERATED_BODY()

public:
	/** Pointer to the movable obstacle. */
	UPROPERTY()
	AActor* Obstacle;

	/** Location from where the movable obstacle commenced its current movement. */
	UPROPERTY()
	FVector StartLocation;

	/** Time required by the movable obstacle to perform its movements. */
	UPROPERTY()
	float TimeRequiredForMovement;
};


/**
 * ManagerOfSublevels defines an object that is used by the OkpaGameMode to manage sublevels of the Okpa game.
 * @author Ejiofor Ifechukwu.
 */
UCLASS()
class OKPA_API UManagerOfSublevels : public UObject
{
	GENERATED_BODY()

public:
	/** Sets default values for this object's properties. */
	UManagerOfSublevels();

	/** Initializes the view target (camera) of this game. */
	void InitializeViewTarget();

	/** Opens a sublevel. */
	void OpenSublevel(int SublevelThatShouldBeOpened);

	/** Closes the current sublevel, if it is already opened. */
	void CloseCurrentSublevel();

	/** Returns the identifier of the current sublevel. */
	int GetIdentifierOfCurrentSublevel() const;

	/** Returns true if the specified actor is a movable obstacle. */
	bool ActorIsMovableObstacle(AActor* Actor) const;

	/** Reverses the direction of the specified actor (if it is a movable obstacle). */
	void ReverseDirectionOfMovableObstacle(AActor* Actor);

protected:
	/** Causes the specified sublevel to be the current sublevel. */
	void SetCurrentSublevel(int NewCurrentSublevel);

	/** Spawns stationary obstacles for the current sublevel. */
	void SpawnStationaryObstaclesForCurrentSublevel();

	/** Spawns movable obstacles for the current sublevel. */
	void SpawnMovableObstaclesForCurrentSublevel();

	/** Spawns a stationary obstacle at the specified location. */
	void SpawnStationaryObstacle(FString NameOfStaticMeshToUseToRenderTheObstacle, FVector Location);

	/** Spawns a movable obstacle to consist of the specified paper flipbook and move continously between the specified initial location and the specified final location with the specified speed. */
	void SpawnMovableObstacle(FString NameOfPaperFlipbookToUseToRenderTheObstacle, FVector InitialLocation, FVector FinalLocation, float Speed = 300);
	
	/** Destroys all the obstacles that have been spawned. */
	void DestroyAllSpawnedObstacles();

	/** Converts an integer number to a name of a static mesh of a stationary obstacle. */
	FString ConvertToNameOfStaticMeshOfStationaryObstacle(int Number) const;

	/** Converts an integer number to a name of a paper flipbook of a movable obstacle. */
	FString ConvertToNameOfPaperFlipbookOfMovableObstacle(int Number) const;

	/** Converts an integer number to a location on the surface of the river where an obstacle can be spawned. */
	FVector ConvertToLocationOnSurfaceOfRiver(int Number) const;

	/** Converts an integer number to a location in the sky where an obstacle can be spawned. */
	FVector ConvertToLocationInTheSky(int Number) const;

	/** Converts an integer number to a speed within the allowable range of movable obstacle speeds. */
	float ConvertToSpeed(int Number) const;

	/** Returns the default rotation that a movable obstacle needs to be oriented to move from the specified initial location to the specified final location. */
	FRotator GetDefaultRotationOfMovableObstacle(const FVector& InitialLocation, const FVector& FinalLocation) const;

	/** Searches for the specified actor from the array of SpawnedMovableObstacles. If found, returns the index where the specified actor is found. Otherwise, returns INDEX_NONE. */
	int GetIndexOfSpawnedMovableObstacle(const AActor* Actor) const;

protected:
	/** Seeded random numbers that are pre-generated when this object is constructed.
		@note Because these pre-generated random numbers are seeded, the same set of random numbers always populates this array every time this object is constructed (as long as the seed does not change). */
	TArray<unsigned> PreGeneratedRandomNumbers;

	/** Identifier of the current sublevel. */
	int CurrentSublevel;

	/** Camera that is used as the view target of the game's world. */
	UPROPERTY()
	AActor* ViewTarget;

	/** Stationary obstacles that have been spawned. */
	UPROPERTY()
	TArray<AActor*> SpawnedStationaryObstacles;

	/** Data about the movable obstacles that have been spawned. */
	UPROPERTY()
	TArray<FDataAboutSpawnedMovableObstacle> SpawnedMovableObstacles;

	/** Names of the static meshes that will be used to render stationary obstacles. */
	static const TArray<FString> StaticMeshesOfStationaryObstacles;

	/** Names of the paper flipbooks that will be used to animate movements of movable obstacles. */
	static const TArray<FString> PaperFlipbooksOfMovableObstacles;
};
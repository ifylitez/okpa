// OkpaLibrary.h: Definitions of macros and static functions that could be used in various parts of the Okpa game.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "UObject/Object.h"
#include "Classes/PaperFlipbook.h"
#include "OkpaLibrary.generated.h"


/**
 * The following macros define constants that could be used in various parts of the Okpa game.
 * @author Ejiofor, Ifechukwu.
 */
#define FOLDER_WHERE_ESSENTIAL_PAPER_FLIPBOOKS_ARE_STORED	TEXT("/Game/PaperFlipbookAnimations/")
#define FOLDER_WHERE_ESSENTIAL_SOUND_WAVES_ARE_STORED		TEXT("/Game/SoundWaves/")
#define FOLDER_WHERE_ESSENTIAL_SPRITES_ARE_STORED			TEXT("/Game/Sprites/OtherEssentialSprites/")
#define FOLDER_WHERE_ESSENTIAL_STATIC_MESHES_ARE_STORED		TEXT("/Game/StaticMeshes/")
#define FOLDER_WHERE_ESSENTIAL_TEXTURES_ARE_STORED			TEXT("/Game/Textures/")
#define FOLDER_WHERE_ESSENTIAL_WIDGET_BLUEPRINTS_ARE_STORED	TEXT("/Game/WidgetBlueprints/")
#define NAME_OF_WEAK_MAGNET									TEXT("Weak Magnet")
#define NAME_OF_STRONG_MAGNET								TEXT("Strong Magnet")
#define NAME_OF_WEAK_PROJECTILE_PATH						TEXT("Half-Length Projectile Path")
#define NAME_OF_STRONG_PROJECTILE_PATH						TEXT("Full-Length Projectile Path")
#define NAME_OF_RIVER										TEXT("River")
#define MINIMUM_OKPA_SPEED_THAT_CAN_CAUSE_RIVER_TO_SPLASH	100
#define TIME_NEEDED_TO_EAT_OKPA_PIECES						2.0
#define COWRIE_REWARD_PER_EATEN_OKPA_PIECE					2
#define WIDTH_OF_SHALLOW_PART_OF_RIVER						150
#define LONGEST_ATTAINABLE_LENGTH_OF_PROJECTILE_PATH		2.3f
#define DEFAULT_LENGTH_OF_PROJECTILE_PATH					-1.0f
#define DEFAULT_RADIUS_OF_MAGNETIC_FIELD					500
#define DEFAULT_STRENGTH_OF_MAGNETIC_FIELD					1500
#define X_LOCATION_OF_CAMERA								0
#define Y_LOCATION_OF_CAMERA								1020
#define Z_LOCATION_OF_CAMERA								360
#define X_LOCATION_OF_LEFT_RIVERBANK						(X_LOCATION_OF_CAMERA - 550)
#define X_LOCATION_OF_RIGHT_RIVERBANK						(X_LOCATION_OF_CAMERA + 400)
#define Y_LOCATION_OF_CATAPULT								(Y_LOCATION_OF_CAMERA - 850)
#define Z_LOCATION_OF_LAND_SURFACE							(Z_LOCATION_OF_CAMERA - 360)
#define Z_LOCATION_OF_RIVER_SURFACE							(Z_LOCATION_OF_LAND_SURFACE - 0)
#define Z_LOCATION_OF_SKY_BOTTOM							(Z_LOCATION_OF_LAND_SURFACE + 50)
#define Z_LOCATION_OF_SKY_TOP								(Z_LOCATION_OF_LAND_SURFACE + 780)
#define LOCATION_OF_CAMERA									FVector(X_LOCATION_OF_CAMERA, Y_LOCATION_OF_CAMERA, Z_LOCATION_OF_CAMERA)
#define LOCATION_OF_BOWL									FVector(X_LOCATION_OF_LEFT_RIVERBANK - 100, Y_LOCATION_OF_CATAPULT, Z_LOCATION_OF_LAND_SURFACE)
#define LOCATION_OF_CATAPULT_INITIALLY						FVector(X_LOCATION_OF_RIGHT_RIVERBANK + 500, Y_LOCATION_OF_CATAPULT, Z_LOCATION_OF_LAND_SURFACE)
#define LOCATION_OF_WHEELBARROW_INITIALLY					FVector(X_LOCATION_OF_RIGHT_RIVERBANK + 500, Y_LOCATION_OF_CATAPULT - 350, Z_LOCATION_OF_LAND_SURFACE)
#define LOCATION_OF_CATAPULT_FINALLY						FVector(X_LOCATION_OF_RIGHT_RIVERBANK + 50, Y_LOCATION_OF_CATAPULT, Z_LOCATION_OF_LAND_SURFACE)
#define LOCATION_OF_WHEELBARROW_FINALLY						FVector(X_LOCATION_OF_RIGHT_RIVERBANK + 50, Y_LOCATION_OF_CATAPULT - 350, Z_LOCATION_OF_LAND_SURFACE)
#define LOCATION_OF_OKPA_PIECE_EATER						FVector(LOCATION_OF_BOWL.X - 80, LOCATION_OF_BOWL.Y, LOCATION_OF_BOWL.Z + 100)
#define ROTATION_OF_WHEELBARROW_WHILE_BEING_PUSHED			FRotator(10, 0, 0)
#define RADIANS_TO_DEGREES_CONVERSION_FACTOR				(180.0f / PI)
#define GetSign(x)											((x) < 0 ? (-1) : (+1))


/**
 * DataAboutOkpaGameBooster defines the data that is stored, by OkpaLibrary, about each booster of the Okpa game.
 * @author Ejiofor, Ifechuwkwu.
 */
USTRUCT(BlueprintType)
struct FDataAboutOkpaGameBooster
{
	GENERATED_BODY()

public:
	/** Name of this booster.
		@note The name of every booster should be unique. */
	UPROPERTY(BlueprintReadWrite)
	FName Name;

	/** Description of this booster. */
	UPROPERTY(BlueprintReadWrite)
	FText Description;

	/** Image of this booster. */
	UPROPERTY(BlueprintReadWrite)
	UObject* Image;

	/** Price of this booster. */
	UPROPERTY(BlueprintReadWrite)
	int Price;
};


/**
 * OkpaLibrary defines various static functions that could be called to perform services in various parts of the Okpa game.
 * @author Ejiofor, Ifechukwu.
 */
UCLASS()
class OKPA_API UOkpaLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/** Breaks the specified string into several lines and returns the broken version. */
	UFUNCTION(BlueprintPure)
	static FString BreakIntoSeveralLines(FString StringBeingBroken, int MaximumCountOfCharactersForOneLine = 15, int MaximumCountOfWordsForOneLine = 3);

	/** Quits the game and prints the reason for quitting game. */
	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"))
	static void QuitGameBecauseOfSomeReason(const FString& ReasonForQuittingGame, UObject* WorldContextObject);

	/** Returns the current game mode if it is an OkpaGameMode. Returns nullptr otherwise. */
	UFUNCTION(BlueprintPure, meta = (WorldContext = "WorldContextObject", Keywords = "getgamemode"))
	static class AOkpaGameMode* GetOkpaGameMode(const UObject* WorldContextObject);

	/** Returns the current HUD if it is an OkpaHUD. Returns nullptr otherwise. */
	static class AOkpaHUD* GetOkpaHUD(const UObject* WorldContextObject);

	/** Returns data about all the boosters that are sold in the marketplace of the Okpa game. */
	static TArray<FDataAboutOkpaGameBooster> GetDataAboutBoostersSoldInMarketplace();

	/** Returns data about the specified booster. */
	static FDataAboutOkpaGameBooster GetDataAboutBooster(const FName& NameOfBooster);

	/** Returns a string that explains that some essential assets could not be loaded. */
	static FString GetStringThatExplainsThatSomeEssentialAssetsAreNotLoadable();

	/** Returns the reference path of the specified asset.
		@note The reference path of an asset is the full path of the location where the asset is stored within the Unreal Engine. */
	static FString GetReferencePathOfAsset(const FString& NameOfAsset);

	/** Returns the world location of the mouse cursor (if mouse input is being used) or the world location that is currently touched from the touch screen (if touch input is being used).
		@note This function returns a zero vector if neither mouse input nor touch input is being used.
		@note The Y-axis of the returned world location is always set to the Y_LOCATION_OF_CATAPULT. */
	static FVector GetWorldLocationWhereTouchInputOrMouseInputOccurred(const UObject* WorldContextObject);

	/** Returns the size of a typical okpa piece. */
	static FVector GetSizeOfTypicalOkpaPiece();

	/** Returns true if the specified name is a valid name of booster. Returns false otherwise. */
	static bool IsValidNameOfBooster(const FName& InName);

	/** Returns true if some essential assets could not be loaded. Return false otherwise.
		@note	Essential assets are those assets that are referenced by some C++ code within the Okpa game. */
	static bool SomeEssentialAssetsAreNotLoadable();

	/** Plays the specified paper flipbook animation at the specified location. */
	static void PlayAnimationAtLocation(const UObject* WorldContextObject, UPaperFlipbook* Animation, FVector Location);

	/** Destroys all actors of the specified class from the specified world. */
	static void DestroyAllActorsOfClass(TSubclassOf<AActor> Class, const UObject* WorldContextObject);

protected:
	/** Returns names of the essential assets that could not be loaded. */
	static FString GetNamesOfEssentialAssetsThatAreNotLoadable();

protected:
	/** Names of all essential paper flipbooks. */
	static const TArray<FString> NamesOfEssentialPaperFlipbooks;

	/** Names of all essential sounds waves. */
	static const TArray<FString> NamesOfEssentialSoundWaves;

	/** Names of all essential sprites. */
	static const TArray<FString> NamesOfEssentialSprites;

	/** Names of all essential static meshes. */
	static const TArray<FString> NamesOfEssentialStaticMeshes;

	/** Names of all essential textures. */
	static const TArray<FString> NamesOfEssentialTextures;

	/** Names of all essential widget blueprint. */
	static const TArray<FString> NamesOfEssentialWidgetBlueprints;

	/** Data about all the boosters that are sold in the marketplace of the Okpa game. */
	static const TArray<FDataAboutOkpaGameBooster> BoostersSoldInMarketplace;
};
// OkpaPiece.h: OkpaPiece class definition.

#pragma once

#include "CoreMinimal.h"
#include "ActorThatCanMove.h"
#include "Components/StaticMeshComponent.h"
#include "OkpaPiece.generated.h"


/**
 * OkpaPiece defines an actor that represents an okpa piece of the Okpa game.
 * @author Ejiofor Ifechukwu.
 */
UCLASS()
class OKPA_API AOkpaPiece : public AActorThatCanMove
{
	GENERATED_BODY()
	
public:
	/** Sets default values for this okpa piece's properties. */
	AOkpaPiece();

	/** Returns the velocity of this okpa piece. */
	virtual FVector GetVelocity() const override;

	/** Performs the processing that is to be performed when this okpa piece begins to overlap another actor. */
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	/** Performs the processing that is to be performed when this okpa piece hits another actor. */
	virtual void NotifyHit(UPrimitiveComponent* MyComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;

protected:
	/** Component that is used to render this okpa piece. */
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* RenderComponent;
};
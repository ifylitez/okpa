// ActorThatCanMove.cpp: ActorThatCanMove class implementation.

#include "ActorThatCanMove.h"
#include "OkpaLibrary.h"
#include "OkpaGameMode.h"

#define DEFAULT_UNIT_TIME 0.015f


AActorThatCanMove::AActorThatCanMove()
	: CurrentVelocity(0, 0, 0), SweepMovementStatus(false), DestinationArrivalIsRelevant(false)
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	PrimaryActorTick.bCanEverTick = true;
}


void AActorThatCanMove::StartMovingToSpecificDestination(const FVector& Destination, float TimeRequired, bool ShouldBeInfluencedByGravity)
{
	CurrentVelocity = (Destination - GetActorLocation()) / (TimeRequired > 0.0 ? TimeRequired : 1.0);
	CurrentDestination = Destination;
	DestinationArrivalIsRelevant = true;
	IsBeingInfluencedByGravity = ShouldBeInfluencedByGravity;
}


void AActorThatCanMove::StartMovingWithNewVelocity(const FVector& NewVelocity, bool ShouldBeInfluencedByGravity)
{
	CurrentVelocity = NewVelocity;
	DestinationArrivalIsRelevant = false;
	IsBeingInfluencedByGravity = ShouldBeInfluencedByGravity;
}


void AActorThatCanMove::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!IsMoving()) {
		return;
	}

	if (IsBeingInfluencedByGravity) {
		AddActorWorldOffset(GetDisplacementUnderInfluenceOfGravity(DeltaTime), SweepMovementStatus);
	}
	else {
		AddActorWorldOffset(GetLinearDisplacement(DeltaTime), SweepMovementStatus);
	}

	if (DestinationArrivalIsRelevant && HasArrivedAtDestination()) {
		SetActorLocation(CurrentDestination);
		StopMoving();
		UOkpaLibrary::GetOkpaGameMode(this)->ProcessActorDestinationArrival(this, CurrentDestination);
	}
}


FVector AActorThatCanMove::GetDisplacementUnderInfluenceOfGravity(float TimeInterval)
{
	float UnitTime = FMath::Min(DEFAULT_UNIT_TIME, TimeInterval);
	int CountOfMicroTicks = TimeInterval / UnitTime;
	FVector Gravity(0, 0, GetWorld()->GetGravityZ());
	FVector Displacement(0, 0, 0);

	for (int Count = 1; Count <= CountOfMicroTicks; ++Count) {
		Displacement += (CurrentVelocity * UnitTime) + (0.5 * Gravity * UnitTime * UnitTime);
		CurrentVelocity.Z += Gravity.Z * UnitTime;
	}

	return Displacement;
}


FVector AActorThatCanMove::GetLinearDisplacement(float TimeInterval) const
{
	return CurrentVelocity * TimeInterval;
}


void AActorThatCanMove::StopMoving()
{
	DestinationArrivalIsRelevant = false;
	IsBeingInfluencedByGravity = false;
	CurrentVelocity = FVector::ZeroVector;
}


bool AActorThatCanMove::IsMoving() const
{
	return !CurrentVelocity.IsNearlyZero() || IsBeingInfluencedByGravity;
}


bool AActorThatCanMove::HasArrivedAtDestination() const
{
	return CurrentDestination.Equals(GetActorLocation(), 1.0) || ((CurrentDestination - GetActorLocation()).GetClampedToSize(-1.0, 1.0) + CurrentVelocity.GetClampedToSize(-1.0, 1.0)).IsNearlyZero(1.0);
}


void AActorThatCanMove::AddVelocity(const FVector& AdditionalVelocity)
{
	CurrentVelocity += AdditionalVelocity;
}


FVector AActorThatCanMove::GetVelocity() const
{
	return CurrentVelocity;
}


void AActorThatCanMove::EnableSweepMovement()
{
	SweepMovementStatus = true;
}


void AActorThatCanMove::DisableSweepMovement()
{
	SweepMovementStatus = false;
}
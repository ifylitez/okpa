// OkpaHUD.h: OkpaHUD class definition.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "OkpaHUD.generated.h"


/**
 * OkpaTutorialState defines constants that are used to identify the states that the Okpa game tutorial can be in.
 * @see		DataAboutOkpaTutorial.
 * @author	Ejiofor Ifechukwu.
 */
UENUM()
enum EOkpaTutorialState
{
	INITIAL_STATE,
	GRIP_STATE,
	PULL_STATE,
	UNGRIP_STATE
};


/**
 * DataAboutOkpaTutorial defines the data that the OkpaHUD stores about the Okpa game tutorial.
 * @note	The Okpa game tutorial is a short tutorial that teaches how to play the Okpa game.
 * @author	Ejiofor Ifechukwu.
 */
USTRUCT()
struct FDataAboutOkpaTutorial
{
	GENERATED_BODY();

public:
	/** Current state of this tutorial. */
	UPROPERTY()
	TEnumAsByte<EOkpaTutorialState> CurrentState;

	/** Time elased since this tutorial entered its current state. */
	UPROPERTY()
	float TimeSpentInCurrentState;

	/** Current text of this tutorial. */
	UPROPERTY()
	FString CurrentText;

	/** Position of the text of this tutorial. */
	FVector2D PositionOfText;

	/** Current position of the finger that is used by this tutorial. */
	UPROPERTY()
	FVector2D CurrentPositionOfFinger;

	/** Initial position of the finger that is used by this tutorial. */
	UPROPERTY()
	FVector2D InitialPositionOfFinger;

	/** Final position of the finger that is used by this tutorial. */
	UPROPERTY()
	FVector2D FinalPositionOfFinger;

	/** Transitions this tutorial to a new state. */
	void TransitionToState(EOkpaTutorialState NewState);
};


/**
 * DataAboutCallout defines the data that the OkpaHUD stores about each callout that it is displays.
 * @author Ejiofor Ifechukwu.
 */
USTRUCT()
struct FDataAboutCallout
{
	GENERATED_BODY()

public:
	/** Text of the callout. */
	UPROPERTY()
	FString Text;

	/** Screen position of the callout. */
	UPROPERTY()
	FVector2D Position;

	/** Number of seconds that has elapsed since the callout started to be displayed. */
	UPROPERTY()
	float Age;
};


/**
 * OkpaHUD defines the HUD that is used to draw on the viewport of the Okpa game.
 * @author Ejiofor Ifechukwu.
 */
UCLASS()
class OKPA_API AOkpaHUD : public AHUD
{
	GENERATED_BODY()

public:
	/** Sets default values of this HUD's properties. */
	AOkpaHUD();

	/** Causes this HUD to display the Okpa game tutorial. */
	void DisplayTutorial();

	/** Causes this HUD to stop displaying the Okpa game tutorial that it is currently displaying. */
	void StopDisplayingTutorial();

	/** Returns true if the Okpa game tutorial is currently being displayed. Returns false otherwise. */
	bool TutorialIsBeingDisplayed() const;

	/** Causes this HUD to display the specified callout at the specified location. */
	void DisplayCallout(const FString& CalloutTextValue, const FVector& CalloutLocation);

	/** Causes this HUD to stop displaying all the callouts that it is currently displaying. */
	void StopDisplayingCurrentCallouts();

	/** Causes this HUD to display the specified fadeout text at the specified location. */
	void DisplayFadeoutText(const FString& FadeoutTextValue, const FVector& FadeoutTextLocation);

	/** Causes this HUD to stop displaying the fadeout text that it is currently displaying. */
	void StopDisplayingCurrentFadeoutTexts();

	/** Causes this HUD to display the specified projectile path. */
	void DisplayProjectilePath(const TArray<FVector>& ProjectilePathThatShouldBeDisplayed);

	/** Causes this HUD to stop displaying the projectile path that it is currently displaying. */
	void StopDisplayingCurrentProjectilePath();

	/** Causes this HUD to display the field of the specified magnet. */
	void DisplayMagneticField(const class AMagnet* MagnetWhoseFieldShouldBeDisplayed);

	/** Causes this HUD to stop displaying the field of the specified magnet. */
	void StopDisplayingMagneticField(const class AMagnet* MagnetWhoseFieldShouldNoLongerBeDisplayed);

	/** Draws this HUD.
		@see AHUD::DrawHUD. */
	virtual void DrawHUD() override;

	/** Performs the processing that is to be performed periodically. */
	virtual void Tick(float DeltaSeconds) override;

protected:
	/** Performs the processing that is to be performed when this HUD is spawned. */
	virtual void BeginPlay() override;

	/** Updates the current tutorial. */
	void UpdateCurrentTutorial(float TimeElapsedSinceLastUpdate);

	/** Updates the current callouts. */
	void UpdateCurrentCallouts(float TimeElapsedSinceLastUpdate);

	/** Updates the current magnetic fields. */
	void UpdateCurrentMagneticFields(float TimeElapsedSinceLastUpdate);

	/** Draws the current tutorial. */
	void DrawCurrentTutorial();

	/** Draws the current callouts. */
	void DrawCurrentCallouts();

	/** Draws the current projectile path.  */
	void DrawCurrentProjectilePath();

	/** Draws the current magnets.  */
	void DrawCurrentMagneticFields();

	/** Overloaded version of AHUD::DrawText but can be used to draw a center-aligned, shadowed text on this HUD. */
	void DrawText(const FString& Text, FLinearColor TextColor, float ScreenX, float ScreenY, bool CentreAlign = false, bool ShadowShouldBeEnabled = false, FLinearColor ShadowColor = FLinearColor::Black, UFont* Font = NULL, float Scale = 1.f, bool bScalePosition = false);

protected:
	/** Texture that is used, by this HUD, to represent the tutorial finger of the Okpa game tutorial. */
	UPROPERTY()
	UTexture2D* FingerTexture;

	/** Texture that is used, by this HUD, to represent the screen position where the tutorial finger presses on. */
	UPROPERTY()
	UTexture2D* FingerPressTexture;

	/** Texture that is used, by this HUD, to represent the background of a callout. */
	UPROPERTY()
	UTexture2D* CalloutBackgroundTexture;

	/** Texture that is used, by this HUD, to represent a point in a projectile path. */
	UPROPERTY()
	UTexture2D* ProjectilePointTexture;

	/** Texture that is used, by this HUD, to represent a point in the edge of a magnetic field. */
	UPROPERTY()
	UTexture2D* MagneticFieldEdgePointTexture;

	/** Flag that indicates whether or not the Okpa game tutorial should be displayed. */
	bool CurrentTutorialShouldBeDisplayed;

	/** Data stored about the tutorial that is currently being displayed by this HUD. */
	FDataAboutOkpaTutorial CurrentTutorial;

	/** Data stored about the callouts that are currently being displayed by this HUD. */
	TArray<FDataAboutCallout> CurrentCallouts;

	/** Screen positions of the points that make up the projectile path that is currently being displayed by this HUD. */
	TArray<FVector2D> CurrentProjectilePath;

	/** Magnets whose fields are currently being displayed by this HUD. */
	TArray<const class AMagnet*> CurrentMagnets;

	/** Rotation that is applied to the render of the magnetic fields displayed by this HUD. */
	float CurrentRotationOfMagneticFieldRender;
};
// OkpaSaveGame.h: OkpaSaveGame class definition.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "OkpaSaveGame.generated.h"


/**
 * OkpaSaveGame defines an object that OkpaGameMode uses to save data about the achievement of the game player.
 * @author Ejiofor , Ifechukwu.
 */
UCLASS()
class OKPA_API UOkpaSaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	/** Sets the count of okpa pieces that were eaten in the specified sublevel by the game player.
		@return True if successful. False otherwise. */
	bool SetCountOfOkpaPiecesThatWereEatenInSublevel(int Sublevel, int NewCountOfOkpaPieces);

	/** Adds some amount of cowries to the cowries that are owned by the game player.
		@return True if successful. False otherwise. */
	bool AddToCowriesOwned(int AmountOfCowriesToAdd);

	/** Remove some amount cowries from the cowries that are owned by the game player.
		@return True if successful. False otherwise. */
	bool RemoveFromCowriesOwned(int AmountOfCowriesToSubtract);

	/** Adds a booster to the boosters that are owned by the game player.
		@return True if successful. False otherwise. */
	bool AddToBoostersOwned(const FName& BoosterToAdd);

	/** Removes a booster from the boosters that are owned by the game player.
		@return True if successful. False otherwise. */
	bool RemoveFromBoostersOwned(const FName& BoosterToRemove);

	/** Returns the identifier of the highest sublevel that has ever been completed by the game player. */
	int GetIdentifierOfHighestSublevelThatHasEverBeenCompleted() const;

	/** Returns the identifier of the last sublevel that was unlocked by the game player. */
	int GetIdentifierOfLastUnlockedSublevel() const;

	/** Returns the highest count of okpa pieces that have ever been eaten in the specified sublevel by the game player. */
	int GetHighestCountOfOkpaPiecesThatHaveEverBeenEatenInSublevel(int Sublevel) const;

	/** Returns the amount of cowries that are currently owned by the game player. */
	int GetAmountOfCowriesOwned() const;

	/** Returns the names of all the boosters that currently owned by the game player. */
	const TArray<FName>& GetNamesOfBoostersOwned() const;

	/** Returns the quantity that is currently owned, of the specified booster, by the game player. */
	int GetQuantityOwnedOfBooster(const FName& NameOfBooster) const;

private:
	/** Highest counts of okpa pieces that have been eaten in various sublevels by the game player. */
	UPROPERTY()
	TArray<int> HighestCountsOfOkpaPiecesThatEverHaveBeenEaten;

	/** Amount of cowries that are currently owned by the game player. */
	UPROPERTY()
	int AmountOfCowriesOwned;

	/** Names of the boosters that are owned by the game player. */
	UPROPERTY()
	TArray<FName> NamesOfBoostersOwned;

	/** Quantities of the boosters that are owned by the game player. */
	UPROPERTY()
	TArray<int> QuantitiesOfBoostersOwned;
};
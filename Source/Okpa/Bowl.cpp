// Bowl.cpp: Bowl class implementation.

#include "Bowl.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/StaticMesh.h"
#include "OkpaLibrary.h"
#include "OkpaGameMode.h"

#define THICKNESS_OF_ENTERANCE_OF_BOWL	0.4
#define THICKNESS_OF_BOTTOM_OF_BOWL		12


ABowl::ABowl()
	: LocationInsideBowl(LOCATION_OF_BOWL)
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> ObjectFinder(*UOkpaLibrary::GetReferencePathOfAsset(TEXT("BowlMesh")));
	LocationInsideBowl.Z += THICKNESS_OF_BOTTOM_OF_BOWL + (UOkpaLibrary::GetSizeOfTypicalOkpaPiece().Z / 2);
	BodyOfBowl = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BodyOfBowl"));
	EnteranceOfBowl = CreateDefaultSubobject<UBoxComponent>(TEXT("EnteranceOfBowl"));

	if (BodyOfBowl != nullptr) {
		BodyOfBowl->SetStaticMesh(ObjectFinder.Object);

		if (RootComponent != nullptr) {
			RootComponent->DestroyComponent();
		}

		RootComponent = BodyOfBowl;
	}

	if (EnteranceOfBowl != nullptr) {
		EnteranceOfBowl->SetNotifyRigidBodyCollision(true);
		EnteranceOfBowl->SetCollisionResponseToAllChannels(ECR_Block);
		EnteranceOfBowl->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		EnteranceOfBowl->SetupAttachment(RootComponent);

		if (BodyOfBowl != nullptr && BodyOfBowl->GetStaticMesh() != nullptr) {
			FVector SizeOfBodyOfBowl = BodyOfBowl->GetStaticMesh()->GetBoundingBox().GetSize();
			EnteranceOfBowl->SetRelativeLocation(FVector(0, 0, SizeOfBodyOfBowl.Z));
			EnteranceOfBowl->SetBoxExtent(FVector(SizeOfBodyOfBowl.X / 2, SizeOfBodyOfBowl.Y / 2, THICKNESS_OF_ENTERANCE_OF_BOWL / 2));
		}
	}
}


void ABowl::NotifyHit(UPrimitiveComponent* MyComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	if (MyComponent == EnteranceOfBowl && Cast<AOkpaPiece>(OtherActor) != nullptr) {
		HoldOkpaPiece(Cast<AOkpaPiece>(OtherActor));
		GetWorldTimerManager().SetTimer(TimerHandleForWhenHeldOkpaPiecesAreBeingEaten, this, &ABowl::FinalizeEatingOfHeldOkpaPieces, TIME_NEEDED_TO_EAT_OKPA_PIECES);
	}
}


void ABowl::HoldOkpaPiece(AOkpaPiece* OkpaPiece)
{
	HeldOkpaPieces.Add(OkpaPiece);
	OkpaPiece->SetActorLocation(LocationInsideBowl);
}


void ABowl::FinalizeEatingOfHeldOkpaPieces()
{
	TArray<AOkpaPiece*> OkpaPiecesThatWereEaten = HeldOkpaPieces;
	HeldOkpaPieces.Empty();

	for (AOkpaPiece* OkpaPiece : OkpaPiecesThatWereEaten) {
		UOkpaLibrary::GetOkpaGameMode(this)->ProcessOkpaPieceEat(OkpaPiece);
	}
}


bool ABowl::ComponentIsEnteranceOfThisBowl(const UPrimitiveComponent* Component) const
{
	return Component == EnteranceOfBowl;
}


FVector ABowl::GetWorldLocationOfEnterance() const
{
	return EnteranceOfBowl->GetComponentLocation();
}


void ABowl::StopHoldingAllHeldOkpaPieces()
{
	TimerHandleForWhenHeldOkpaPiecesAreBeingEaten.Invalidate();
	HeldOkpaPieces.Empty();
}
// Bowl.h: Bowl class definition.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Public/TimerManager.h"
#include "Components/BoxComponent.h"
#include "OkpaPiece.h"
#include "Bowl.generated.h"


/**
 * Bowl defines an actor that okpa pieces can be targeted at.
 * @author Ejiofor Ifechukwu.
 */
UCLASS()
class OKPA_API ABowl : public AActor
{
	GENERATED_BODY()
	
public:	
	/** Sets default values for this bowl's properties. */
	ABowl();

	/** Performs processing that is to be performed when this bowl hits (or is hit by) another actor. */
	virtual void NotifyHit(UPrimitiveComponent* MyComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;

	/** Returns true if the specified component is the enterance of this bowl. Returns false otherwise. */
	bool ComponentIsEnteranceOfThisBowl(const UPrimitiveComponent* Component) const;

	/** Returns the world location of the enterance of this bowl. */
	FVector GetWorldLocationOfEnterance() const;

	/** Causes this bowl to stop holding all the okpa pieces that it is currently holding. */
	void StopHoldingAllHeldOkpaPieces();

protected:
	/** Causes this bowl to hold the specified okpa piece. */
	void HoldOkpaPiece(AOkpaPiece* OkpaPiece);

	/** Performs the processing that is to be performed when finalizing eating of held okpa pieces. */
	void FinalizeEatingOfHeldOkpaPieces();

protected:
	/** Component that is used to render the body of this bowl. */
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* BodyOfBowl;

	/** Component that is used to represent the enterance of this bowl. */
	UPROPERTY(VisibleAnywhere)
	UBoxComponent* EnteranceOfBowl;

	/** Okpa pieces that are being held inside this bowl. */
	UPROPERTY()
	TArray<AOkpaPiece*> HeldOkpaPieces;

	/** Timer handle for tracking time when held okpa pieces are being eaten. */
	FTimerHandle TimerHandleForWhenHeldOkpaPiecesAreBeingEaten;

	/** Location where okpa pieces should be held inside this bowl. */
	FVector LocationInsideBowl;
};
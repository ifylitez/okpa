// TrackerOfOkpaPieces.cpp: TrackerOfOkpaPieces class implementation.

#include "TrackerOfOkpaPieces.h"


void UTrackerOfOkpaPieces::TrackAsInsideWheelbarrow(AOkpaPiece* OkpaPieceThatShouldBeTracked)
{
	StopTracking(OkpaPieceThatShouldBeTracked);
	OkpaPiecesThatAreInsideWheelbarrow.Add(OkpaPieceThatShouldBeTracked);
}


void UTrackerOfOkpaPieces::StopTracking(AOkpaPiece* OkpaPieceToStopTracking)
{
	OkpaPiecesThatAreInsideWheelbarrow.Remove(OkpaPieceToStopTracking);
	OkpaPieceThatAreInsideCatapultTrigger.Remove(OkpaPieceToStopTracking);
	OkpaPieceThatAreLaunched.Remove(OkpaPieceToStopTracking);
	OkpaPiecesThatAreInsideBowl.Remove(OkpaPieceToStopTracking);
	OkpaPiecesThatAreLitteredButNotFullyStopped.Remove(OkpaPieceToStopTracking);
	OkpaPiecesThatAreLitteredAndFullyStopped.Remove(OkpaPieceToStopTracking);
}


void UTrackerOfOkpaPieces::StopTrackingFromInsideBowl(AOkpaPiece* OkpaPieceToStopTracking)
{
	OkpaPiecesThatAreInsideBowl.Remove(OkpaPieceToStopTracking);
}


void UTrackerOfOkpaPieces::StopTrackingAllTrackedOkpaPieces()
{
	OkpaPiecesThatAreInsideWheelbarrow.Empty();
	OkpaPieceThatAreInsideCatapultTrigger.Empty();
	OkpaPieceThatAreLaunched.Empty();
	OkpaPiecesThatAreInsideBowl.Empty();
	OkpaPiecesThatAreLitteredButNotFullyStopped.Empty();
	OkpaPiecesThatAreLitteredAndFullyStopped.Empty();
}


void UTrackerOfOkpaPieces::TrackAsInsideCatapultTrigger(AOkpaPiece* OkpaPieceThatShouldBeTracked)
{
	StopTracking(OkpaPieceThatShouldBeTracked);
	OkpaPieceThatAreInsideCatapultTrigger.Add(OkpaPieceThatShouldBeTracked);
}


void UTrackerOfOkpaPieces::TrackAsLaunched(AOkpaPiece* OkpaPieceThatShouldBeTracked)
{
	StopTracking(OkpaPieceThatShouldBeTracked);
	OkpaPieceThatAreLaunched.Add(OkpaPieceThatShouldBeTracked);
}


void UTrackerOfOkpaPieces::TrackAsInsideBowl(AOkpaPiece* OkpaPieceThatShouldBeTracked)
{
	StopTracking(OkpaPieceThatShouldBeTracked);
	OkpaPiecesThatAreInsideBowl.Add(OkpaPieceThatShouldBeTracked);
}


void UTrackerOfOkpaPieces::TrackAsLittered(AOkpaPiece* OkpaPieceThatShouldBeTracked)
{
	StopTracking(OkpaPieceThatShouldBeTracked);

	if (OkpaPieceThatShouldBeTracked->GetVelocity().IsNearlyZero()) {
		OkpaPiecesThatAreLitteredAndFullyStopped.Add(OkpaPieceThatShouldBeTracked);
	}
	else {
		OkpaPiecesThatAreLitteredButNotFullyStopped.Add(OkpaPieceThatShouldBeTracked);
	}
}


void UTrackerOfOkpaPieces::TrackAsLitteredAndFullyStopped(AOkpaPiece* OkpaPieceThatShouldBeTracked)
{
	StopTracking(OkpaPieceThatShouldBeTracked);
	OkpaPiecesThatAreLitteredAndFullyStopped.Add(OkpaPieceThatShouldBeTracked);
}


void UTrackerOfOkpaPieces::TrackAsLitteredButNotFullyStopped(AOkpaPiece* OkpaPieceThatShouldBeTracked)
{
	StopTracking(OkpaPieceThatShouldBeTracked);
	OkpaPiecesThatAreLitteredButNotFullyStopped.Add(OkpaPieceThatShouldBeTracked);
}


TArray<AOkpaPiece*> UTrackerOfOkpaPieces::GetOkpaPiecesThatAreLitteredButNotFullyStopped() const
{
	return OkpaPiecesThatAreLitteredButNotFullyStopped;
}


bool UTrackerOfOkpaPieces::OkpaPieceIsInsideBowl(AOkpaPiece* OkpaPiece) const
{
	return OkpaPiecesThatAreInsideBowl.Find(OkpaPiece) != INDEX_NONE;
}


bool UTrackerOfOkpaPieces::OkpaPieceIsInsideCatapultTrigger(AOkpaPiece* OkpaPiece) const
{
	return OkpaPieceThatAreInsideCatapultTrigger.Find(OkpaPiece) != INDEX_NONE;
}


bool UTrackerOfOkpaPieces::OkpaPieceIsInsideWheelbarrow(AOkpaPiece* OkpaPiece) const
{
	return OkpaPiecesThatAreInsideWheelbarrow.Find(OkpaPiece) != INDEX_NONE;
}


bool UTrackerOfOkpaPieces::OkpaPieceIsLaunched(AOkpaPiece* OkpaPiece) const
{
	return OkpaPieceThatAreLaunched.Find(OkpaPiece) != INDEX_NONE;
}


bool UTrackerOfOkpaPieces::OkpaPieceIsLittered(AOkpaPiece* OkpaPiece) const
{
	return OkpaPiecesThatAreLitteredButNotFullyStopped.Find(OkpaPiece) != INDEX_NONE || OkpaPiecesThatAreLitteredAndFullyStopped.Find(OkpaPiece) != INDEX_NONE;
}


bool UTrackerOfOkpaPieces::AllOkpaPiecesHaveLanded() const
{
	return OkpaPiecesThatAreInsideWheelbarrow.Num() == 0 && OkpaPieceThatAreInsideCatapultTrigger.Num() == 0 && OkpaPieceThatAreLaunched.Num() == 0;
}
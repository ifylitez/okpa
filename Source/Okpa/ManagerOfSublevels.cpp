// ManagerOfSublevels.cpp: ManagerOfSublevels class implementation.

#include "ManagerOfSublevels.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h"
#include "Engine/StaticMesh.h"
#include "Classes/PaperFlipbookComponent.h"
#include "CatapultTrigger.h"

#define SEED_OF_PRE_GENERATED_RANDOM_NUMBERS			1
#define COUNT_OF_PRE_GENERATED_RANDOM_NUMBERS			128
#define LARGEST_RANDOM_NUMBER_THAT_CAN_BE_PRE_GENERATED	10000
#define MINIMUM_SPEED_OF_MOVABLE_OBSTACLES				100
#define MAXIMUM_SPEED_OF_MOVABLE_OBSTACLES				800
#define MINIMUM_DISPLACEMENT_OF_MOVABLE_OBSTACLES		300


const TArray<FString> UManagerOfSublevels::StaticMeshesOfStationaryObstacles = { TEXT("ChristmasTreeMesh"),
	TEXT("CoconutTreeMesh"), TEXT("MangoTreeMesh"), TEXT("WoodMesh"), TEXT("RockMesh"), TEXT("TreeStumpMesh") };

const TArray<FString> UManagerOfSublevels::PaperFlipbooksOfMovableObstacles = { TEXT("BatFlyAnimation"),
	TEXT("DoveFlyAnimation"), TEXT("RavenFlyAnimation") };


UManagerOfSublevels::UManagerOfSublevels()
	: ViewTarget(nullptr), CurrentSublevel(FIRST_SUBLEVEL)
{
	FMath::SRandInit(SEED_OF_PRE_GENERATED_RANDOM_NUMBERS);

	for (int i = 1; i <= COUNT_OF_PRE_GENERATED_RANDOM_NUMBERS; ++i) {
		PreGeneratedRandomNumbers.Add(FMath::SRand() * LARGEST_RANDOM_NUMBER_THAT_CAN_BE_PRE_GENERATED);
	}
}


void UManagerOfSublevels::SetCurrentSublevel(int NewCurrentSublevel)
{
	CurrentSublevel = FMath::Max(NewCurrentSublevel, static_cast<int>(FIRST_SUBLEVEL));
}


int UManagerOfSublevels::GetIdentifierOfCurrentSublevel() const
{
	return CurrentSublevel;
}


void UManagerOfSublevels::InitializeViewTarget()
{
	if (ViewTarget != nullptr) {
		return;
	}

	ViewTarget = GetWorld()->SpawnActor<ACameraActor>();

	if (ViewTarget != nullptr) {
		ViewTarget->SetActorLocationAndRotation(LOCATION_OF_CAMERA, FRotator(0, -90, 0));
		GetWorld()->GetFirstPlayerController()->SetViewTarget(ViewTarget);
	}
}


void UManagerOfSublevels::OpenSublevel(int SublevelThatShouldBeOpened)
{
	CloseCurrentSublevel();
	SetCurrentSublevel(SublevelThatShouldBeOpened);
	SpawnStationaryObstaclesForCurrentSublevel();
	SpawnMovableObstaclesForCurrentSublevel();
}


void UManagerOfSublevels::CloseCurrentSublevel()
{
	DestroyAllSpawnedObstacles();
}


void UManagerOfSublevels::DestroyAllSpawnedObstacles()
{
	for (int i = 0; i < SpawnedStationaryObstacles.Num(); ++i) {
		SpawnedStationaryObstacles[i]->Destroy();
	}

	for (int i = 0; i < SpawnedMovableObstacles.Num(); ++i) {
		SpawnedMovableObstacles[i].Obstacle->Destroy();
	}

	SpawnedStationaryObstacles.Empty();
	SpawnedMovableObstacles.Empty();
}


void UManagerOfSublevels::SpawnStationaryObstaclesForCurrentSublevel()
{
	if (CurrentSublevel == FIRST_SUBLEVEL || CurrentSublevel == SECOND_SUBLEVEL) {
		return;
	}

	int Index;
	FString NameOfStaticMesh;
	FVector SpawnLocation;
	int CountOfStationaryObstaclesToSpawn = (CurrentSublevel == THIRD_SUBLEVEL) ? 1 : (CurrentSublevel / 3);

	for (int Count = 1; Count <= CountOfStationaryObstaclesToSpawn; ++Count) {
		Index = (Count + (CurrentSublevel + Count) * Count) % PreGeneratedRandomNumbers.Num();
		NameOfStaticMesh = ConvertToNameOfStaticMeshOfStationaryObstacle(PreGeneratedRandomNumbers[Index]);
		SpawnLocation = ConvertToLocationOnSurfaceOfRiver(PreGeneratedRandomNumbers[Index]);

		if (CurrentSublevel == THIRD_SUBLEVEL) {
			SpawnLocation.X = X_LOCATION_OF_RIGHT_RIVERBANK - WIDTH_OF_SHALLOW_PART_OF_RIVER;
		}

		SpawnStationaryObstacle(NameOfStaticMesh, SpawnLocation);
	}
}


FString UManagerOfSublevels::ConvertToNameOfStaticMeshOfStationaryObstacle(int Number) const
{
	return StaticMeshesOfStationaryObstacles[Number % StaticMeshesOfStationaryObstacles.Num()];
}


FVector UManagerOfSublevels::ConvertToLocationOnSurfaceOfRiver(int Number) const
{
	int XLocation = X_LOCATION_OF_LEFT_RIVERBANK + WIDTH_OF_SHALLOW_PART_OF_RIVER + (Number % (FMath::Abs(X_LOCATION_OF_LEFT_RIVERBANK - X_LOCATION_OF_RIGHT_RIVERBANK) - (2 * WIDTH_OF_SHALLOW_PART_OF_RIVER)));
	return FVector(XLocation, Y_LOCATION_OF_CATAPULT, Z_LOCATION_OF_RIVER_SURFACE);
}


void UManagerOfSublevels::SpawnStationaryObstacle(FString NameOfStaticMeshToUseToRenderTheObstacle, FVector Location)
{
	AStaticMeshActor* StationaryObstacle = nullptr;

	if (GetWorld()) {
		StationaryObstacle = GetWorld()->SpawnActor<AStaticMeshActor>(Location, FRotator::ZeroRotator);
	}

	if (StationaryObstacle != nullptr) {
		StationaryObstacle->GetStaticMeshComponent()->SetMobility(EComponentMobility::Stationary);
		StationaryObstacle->GetStaticMeshComponent()->SetStaticMesh(LoadObject<UStaticMesh>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(NameOfStaticMeshToUseToRenderTheObstacle)));
		SpawnedStationaryObstacles.Add(StationaryObstacle);
	}
}


void UManagerOfSublevels::SpawnMovableObstaclesForCurrentSublevel()
{
	if (CurrentSublevel == FIRST_SUBLEVEL || CurrentSublevel == THIRD_SUBLEVEL) {
		return;
	}

	int Index;
	FString NameOfPaperFlipbook;
	FVector SpawnLocation, DestinationLocation;
	float Speed;
	int CountOfMovableObstaclesToSpawn = (CurrentSublevel == SECOND_SUBLEVEL) ? 1 : (CurrentSublevel / 4);

	for (int Count = 1; Count <= CountOfMovableObstaclesToSpawn; ++Count) {
		Index = (Count + (CurrentSublevel + Count) * Count) % PreGeneratedRandomNumbers.Num();
		NameOfPaperFlipbook = ConvertToNameOfPaperFlipbookOfMovableObstacle(PreGeneratedRandomNumbers[Index]);

		Index = (CurrentSublevel * Count + 1) % PreGeneratedRandomNumbers.Num();
		SpawnLocation = ConvertToLocationInTheSky(PreGeneratedRandomNumbers[Index]);

		Index =(CurrentSublevel * Count + 2) % PreGeneratedRandomNumbers.Num();
		DestinationLocation = ConvertToLocationInTheSky(PreGeneratedRandomNumbers[Index]);

		Index = (CurrentSublevel * Count) % PreGeneratedRandomNumbers.Num();
		Speed = ConvertToSpeed(PreGeneratedRandomNumbers[Index]);

		if (CurrentSublevel == SECOND_SUBLEVEL) {
			SpawnLocation.Z = Z_LOCATION_OF_SKY_TOP;
			DestinationLocation.Z = Z_LOCATION_OF_SKY_BOTTOM;
		}

		SpawnMovableObstacle(NameOfPaperFlipbook, SpawnLocation, DestinationLocation, Speed);
	}
}


FString UManagerOfSublevels::ConvertToNameOfPaperFlipbookOfMovableObstacle(int Number) const
{
	return PaperFlipbooksOfMovableObstacles[Number % PaperFlipbooksOfMovableObstacles.Num()];
}


FVector UManagerOfSublevels::ConvertToLocationInTheSky(int Number) const
{
	int XLocation = X_LOCATION_OF_LEFT_RIVERBANK + (Number % FMath::Abs(X_LOCATION_OF_LEFT_RIVERBANK - X_LOCATION_OF_RIGHT_RIVERBANK));
	int ZLocation = Z_LOCATION_OF_SKY_BOTTOM + (Number % FMath::Abs(Z_LOCATION_OF_SKY_TOP - Z_LOCATION_OF_SKY_BOTTOM));
	return FVector(XLocation, Y_LOCATION_OF_CATAPULT, ZLocation);
}


float UManagerOfSublevels::ConvertToSpeed(int Number) const
{
	return MINIMUM_SPEED_OF_MOVABLE_OBSTACLES + (Number % (MAXIMUM_SPEED_OF_MOVABLE_OBSTACLES - MINIMUM_SPEED_OF_MOVABLE_OBSTACLES));
}


void UManagerOfSublevels::SpawnMovableObstacle(FString NameOfPaperFlipbookToUseToRenderTheObstacle, FVector InitialLocation, FVector FinalLocation, float Speed)
{
	AActorThatCanMove* MovableObstacle = nullptr;
	UPaperFlipbookComponent* PaperFlipbookComponent = nullptr;

	if (FVector::PointsAreNear(InitialLocation, FinalLocation, MINIMUM_DISPLACEMENT_OF_MOVABLE_OBSTACLES)) {
		FinalLocation.X = LOCATION_OF_BOWL.X;
	}

	if (FMath::IsNearlyZero(Speed)) {
		Speed = MINIMUM_SPEED_OF_MOVABLE_OBSTACLES;
	}

	if (GetWorld() != nullptr) {
		MovableObstacle = GetWorld()->SpawnActor<AActorThatCanMove>(InitialLocation, GetDefaultRotationOfMovableObstacle(InitialLocation, FinalLocation));
	}

	if (MovableObstacle != nullptr) {
		PaperFlipbookComponent = NewObject<UPaperFlipbookComponent>(MovableObstacle);
	}

	if (MovableObstacle != nullptr && PaperFlipbookComponent != nullptr) {
		PaperFlipbookComponent->RegisterComponent();
		PaperFlipbookComponent->AttachToComponent(MovableObstacle->GetRootComponent(), FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
		PaperFlipbookComponent->SetMobility(EComponentMobility::Movable);
		PaperFlipbookComponent->SetFlipbook(LoadObject<UPaperFlipbook>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(NameOfPaperFlipbookToUseToRenderTheObstacle)));
		MovableObstacle->StartMovingToSpecificDestination(FinalLocation, FVector::Distance(InitialLocation, FinalLocation) / Speed);
		SpawnedMovableObstacles.Add({ MovableObstacle, InitialLocation, FVector::Distance(InitialLocation, FinalLocation) / Speed });
	}
}


FRotator UManagerOfSublevels::GetDefaultRotationOfMovableObstacle(const FVector& InitialLocation, const FVector& FinalLocation) const
{
	FRotator DefaultRotation(0);

	DefaultRotation.Pitch = RADIANS_TO_DEGREES_CONVERSION_FACTOR * FMath::Acos((FinalLocation.X - InitialLocation.X) / FVector::Distance(FinalLocation, InitialLocation));

	if (DefaultRotation.Pitch > 90.0f) {
		DefaultRotation.Pitch = 180.0f - DefaultRotation.Pitch;
		DefaultRotation.Yaw = 180.0f;
	}

	DefaultRotation *= GetSign(FinalLocation.Z - InitialLocation.Z);

	return DefaultRotation;
}


bool UManagerOfSublevels::ActorIsMovableObstacle(AActor* Actor) const
{
	return GetIndexOfSpawnedMovableObstacle(Actor) != INDEX_NONE;
}


int UManagerOfSublevels::GetIndexOfSpawnedMovableObstacle(const AActor* Actor) const
{
	for (int Index = 0; Index < SpawnedMovableObstacles.Num(); ++Index) {
		if (SpawnedMovableObstacles[Index].Obstacle == Actor) {
			return Index;
		}
	}

	return INDEX_NONE;
}


void UManagerOfSublevels::ReverseDirectionOfMovableObstacle(AActor* Actor)
{
	if (!ActorIsMovableObstacle(Actor) || Cast<AActorThatCanMove>(Actor) == nullptr) {
		return;
	}

	int Index = GetIndexOfSpawnedMovableObstacle(Actor);
	Actor->AddActorLocalRotation(FRotator(-180, 0, 180));
	Cast<AActorThatCanMove>(Actor)->StartMovingToSpecificDestination(SpawnedMovableObstacles[Index].StartLocation, SpawnedMovableObstacles[Index].TimeRequiredForMovement);
	SpawnedMovableObstacles[Index].StartLocation = Actor->GetActorLocation();
}
// OkpaGameMode.cpp: OkpaGameMode class implementaion.

#include "OkpaGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "Components/AudioComponent.h"
#include "Classes/PaperSprite.h"
#include "Classes/PaperFlipbookComponent.h"
#include "Classes/PaperFlipbookActor.h"
#include "CatapultTrigger.h"
#include "Magnet.h"
#include "OkpaHUD.h"
#include "OkpaLibrary.h"

#define TARGET_HIT_TEXT														TEXT("Yes! Target hit.")
#define TARGET_MISS_TEXT													TEXT("Ouch! Target miss.")
#define WHEELBARROW_ARRIVAL_TEXT											TEXT("Freshly cooked okpa has arrived!")
#define TIME_LAG_BEFORE_GAME_SESSION_SHOULD_END								3
#define COUNT_OF_OKPA_PIECES_TO_START_GAME_SESSION_WITH						6
#define NAME_OF_SLOT_FOR_SAVING_GAME										TEXT("GamePlayerAchievement")
#define INDEX_OF_SLOT_FOR_SAVING_GAME										0
#define GamePlayerAchievementIsLoadableFromSlot								(Cast<UOkpaSaveGame>(UGameplayStatics::LoadGameFromSlot(NAME_OF_SLOT_FOR_SAVING_GAME, INDEX_OF_SLOT_FOR_SAVING_GAME)))
#define OnlyOneOkpaPieceHasMovedFromMainWheelbarrowToMainCatapultTrigger	(1 + MainWheelbarrow->GetNumberOfActorsThatAreBeingHeld() == MainWheelbarrow->GetCapacity())
#define AtLeastOneOkpaPieceHasLandedInCurrentGameSession					(FMath::IsNearlyZero(TimeSinceOkpaPieceLastLandedInCurrentGameSession) || TimeSinceOkpaPieceLastLandedInCurrentGameSession > 0)


const TArray<FString> AOkpaGameMode::TextsThatEmphasizeDeliciousnessOfOkpa = {
	TEXT("Hmm! how delicious okpa tastes!"),
	TEXT("'Okpa na ato uto' (Okpa tastes sweet)."),
	TEXT("I love eating okpa, anytime!"),
	TEXT("No other food tastes as sweet as okpa!"),
	TEXT("Okpa. So sweet, yet so nutritious!"),
	TEXT("Yummy Okpa! So Yummy!"),
	TEXT("I can never get tired of eating okpa!"),
	TEXT("Tastes exactly like \"Okpa Nsukka!\""),
	TEXT("Sweet! Very Sweet!"),
	TEXT("Hmm.... What a delicacy!")
};


AOkpaGameMode::AOkpaGameMode()
	: GameSessionIsCurrentlyInProgress(false), CountOfOkpaPiecesEatenDuringCurrentGameSession(-1), TimeSinceOkpaPieceLastLandedInCurrentGameSession(-1)
{
	PrimaryActorTick.bCanEverTick = true;
	DefaultPawnClass = ACatapultTrigger::StaticClass();
	HUDClass = AOkpaHUD::StaticClass();
}


void AOkpaGameMode::BeginPlay()
{
	Super::BeginPlay();

#if WITH_EDITOR
	if (!DefaultPawnClass->IsChildOf<ACatapultTrigger>()) {
		UOkpaLibrary::QuitGameBecauseOfSomeReason(FString("Default pawn class is not a subclass of CatapultTrigger C++ class."), this);
		return;
	}

	if (!HUDClass->IsChildOf<AOkpaHUD>()) {
		UOkpaLibrary::QuitGameBecauseOfSomeReason(FString("HUD class is not a subclass of OkpaHUD C++ class."), this);
		return;
	}

	if (UOkpaLibrary::SomeEssentialAssetsAreNotLoadable()) {
		UOkpaLibrary::QuitGameBecauseOfSomeReason(UOkpaLibrary::GetStringThatExplainsThatSomeEssentialAssetsAreNotLoadable(), this);
		return;
	}
#endif
	
	bool GameInitializationStatus = InitializeGame();

	if (GameInitializationStatus == false) {
		UOkpaLibrary::QuitGameBecauseOfSomeReason(FString("An unexpected error was encountered while initializing the game."), this);
		return;
	}

	DisplayHomeScreen();
}


bool AOkpaGameMode::InitializeGame()
{
	if (!GetWorld() || !GetWorld()->GetFirstPlayerController()) {
		return false;
	}

	UWorld* World = GetWorld();
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();

	PlayerController->bShowMouseCursor = true;
	PlayerController->bEnableClickEvents = true;
	PlayerController->bEnableTouchEvents = true;

	GameSessionScreen = CreateWidget(World, LoadClass<UUserWidget>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("GameSessionScreen"))));
	PauseScreen = CreateWidget(World, LoadClass<UUserWidget>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("PauseScreen"))));
	SuccessScreen = CreateWidget(World, LoadClass<UUserWidget>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("SuccessScreen"))));
	FailureScreen = CreateWidget(World, LoadClass<UUserWidget>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("FailureScreen"))));

	MainWheelbarrow = World->SpawnActor<AWheelbarrow>(LOCATION_OF_WHEELBARROW_INITIALLY, FRotator::ZeroRotator);
	MainCatapult = World->SpawnActor<ACatapult>(LOCATION_OF_CATAPULT_INITIALLY, FRotator::ZeroRotator);
	MainBowl = World->SpawnActor<ABowl>(LOCATION_OF_BOWL, FRotator::ZeroRotator);
	MainAmbientSound = World->SpawnActor<AAmbientSound>();

	ManagerOfSublevels = NewObject<UManagerOfSublevels>(this);
	TrackerOfOkpaPieces = NewObject<UTrackerOfOkpaPieces>(this);

	GamePlayerAchievement = GetGamePlayerInitialAchievement();

	if (GameSessionScreen == nullptr || PauseScreen == nullptr ||
		SuccessScreen == nullptr || FailureScreen == nullptr ||
		MainWheelbarrow == nullptr || MainCatapult == nullptr ||
		MainBowl == nullptr || MainAmbientSound == nullptr ||
		ManagerOfSublevels == nullptr || TrackerOfOkpaPieces == nullptr ||
		GamePlayerAchievement == nullptr)
	{
		return false;
	}

	ManagerOfSublevels->InitializeViewTarget();
	return true;
}


UOkpaSaveGame* AOkpaGameMode::GetGamePlayerInitialAchievement() const
{
	UOkpaSaveGame* GamePlayerInitialProgress;

	if (GamePlayerAchievementIsLoadableFromSlot) {
		GamePlayerInitialProgress = Cast<UOkpaSaveGame>(UGameplayStatics::LoadGameFromSlot(NAME_OF_SLOT_FOR_SAVING_GAME, INDEX_OF_SLOT_FOR_SAVING_GAME));
	}
	else {
		GamePlayerInitialProgress = Cast<UOkpaSaveGame>(UGameplayStatics::CreateSaveGameObject(UOkpaSaveGame::StaticClass()));
		UGameplayStatics::SaveGameToSlot(GamePlayerInitialProgress, NAME_OF_SLOT_FOR_SAVING_GAME, INDEX_OF_SLOT_FOR_SAVING_GAME);
	}

	return GamePlayerInitialProgress;
}


void AOkpaGameMode::DisplayHomeScreen()
{
	UUserWidget* HomeScreen;

	UWidgetLayoutLibrary::RemoveAllWidgets(this);
	HomeScreen = CreateWidget(GetWorld(), LoadClass<UUserWidget>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("HomeScreen"))));
	
	if (HomeScreen != nullptr) {
		HomeScreen->AddToViewport();
	}
}


void AOkpaGameMode::DisplayMenuScreen()
{
	UUserWidget* MenuScreen;

	CloseCurrentSublevel();
	UWidgetLayoutLibrary::RemoveAllWidgets(this);
	MenuScreen = CreateWidget(GetWorld(), LoadClass<UUserWidget>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("MenuScreen"))));

	if (MenuScreen != nullptr) {
		MenuScreen->AddToViewport();
	}

	MainAmbientSound->GetAudioComponent()->SetSound(LoadObject<USoundBase>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("MenuMusic"))));
	MainAmbientSound->Play();
}


void AOkpaGameMode::DisplayStoryScreen()
{
	UUserWidget* StoryScreen;

	UWidgetLayoutLibrary::RemoveAllWidgets(this);
	StoryScreen = CreateWidget(GetWorld(), LoadClass<UUserWidget>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("StoryScreen"))));

	if (StoryScreen != nullptr) {
		StoryScreen->AddToViewport();
	}
}


void AOkpaGameMode::DisplayMarketplaceScreen()
{
	UUserWidget* MarketplaceScreen = CreateWidget(GetWorld(), LoadClass<UUserWidget>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("MarketplaceScreen"))));

	if (MarketplaceScreen != nullptr) {
		MarketplaceScreen->AddToViewport();
	}
}


bool AOkpaGameMode::PurchaseBooster(const FName& BoosterToPurchase)
{
	bool CowrieRemovalStatus, BoosterAdditionStatus, SaveGameStatus;
	int PriceOfBoosterToPurchase = UOkpaLibrary::GetDataAboutBooster(BoosterToPurchase).Price;

	if (PriceOfBoosterToPurchase > GamePlayerAchievement->GetAmountOfCowriesOwned()) {
		return false;
	}

	CowrieRemovalStatus = GamePlayerAchievement->RemoveFromCowriesOwned(PriceOfBoosterToPurchase);
	BoosterAdditionStatus = GamePlayerAchievement->AddToBoostersOwned(BoosterToPurchase);

	if (CowrieRemovalStatus == false || BoosterAdditionStatus == false) {
		GamePlayerAchievement = Cast<UOkpaSaveGame>(UGameplayStatics::LoadGameFromSlot(NAME_OF_SLOT_FOR_SAVING_GAME, INDEX_OF_SLOT_FOR_SAVING_GAME));
		return false;
	}

	SaveGameStatus = UGameplayStatics::SaveGameToSlot(GamePlayerAchievement, NAME_OF_SLOT_FOR_SAVING_GAME, INDEX_OF_SLOT_FOR_SAVING_GAME);

	if (SaveGameStatus == false) {
		GamePlayerAchievement = Cast<UOkpaSaveGame>(UGameplayStatics::LoadGameFromSlot(NAME_OF_SLOT_FOR_SAVING_GAME, INDEX_OF_SLOT_FOR_SAVING_GAME));
		return false;
	}

	UpdateGameSessionScreen();
	return true;
}


void AOkpaGameMode::RemoveFromBoostersOwnedByGamePlayer(const FName & BoosterToRemove)
{
	GamePlayerAchievement->RemoveFromBoostersOwned(BoosterToRemove);
	UGameplayStatics::SaveGameToSlot(GamePlayerAchievement, NAME_OF_SLOT_FOR_SAVING_GAME, INDEX_OF_SLOT_FOR_SAVING_GAME);
	UpdateGameSessionScreen();
}


void AOkpaGameMode::DeployBooster(const FName& BoosterToDeploy)
{
	if (BoosterToDeploy == NAME_OF_WEAK_MAGNET) {
		AMagnet* Magnet = GetWorld()->SpawnActor<AMagnet>(MainBowl->GetWorldLocationOfEnterance(), FRotator::ZeroRotator);
		Magnet->SetStrengthOfField(0.5 * DEFAULT_STRENGTH_OF_MAGNETIC_FIELD);
		Magnet->SetRadiusOfField(0.5 * DEFAULT_RADIUS_OF_MAGNETIC_FIELD);
		Magnet->SetRenderSprite(LoadObject<UPaperSprite>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("WeakMagnetSprite"))));
	}
	else if (BoosterToDeploy == NAME_OF_STRONG_MAGNET) {
		AMagnet* Magnet = GetWorld()->SpawnActor<AMagnet>(MainBowl->GetWorldLocationOfEnterance(), FRotator::ZeroRotator);
		Magnet->SetRenderSprite(LoadObject<UPaperSprite>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("StrongMagnetSprite"))));
	}
	else if (BoosterToDeploy == NAME_OF_WEAK_PROJECTILE_PATH) {
		MainCatapult->SetLengthOfProjectilePathOfTrigger(0.5 * LONGEST_ATTAINABLE_LENGTH_OF_PROJECTILE_PATH);
	}
	else if (BoosterToDeploy == NAME_OF_STRONG_PROJECTILE_PATH) {
		MainCatapult->SetLengthOfProjectilePathOfTrigger(LONGEST_ATTAINABLE_LENGTH_OF_PROJECTILE_PATH);
	}
}


void AOkpaGameMode::OpenSublevel(int IdentifierOfSublevelThatShouldBeOpened)
{
	ManagerOfSublevels->OpenSublevel(IdentifierOfSublevelThatShouldBeOpened);
	StartGameSession();
}


void AOkpaGameMode::StartGameSession()
{
	InsertNewOkpaPiecesIntoMainWheelbarrow(COUNT_OF_OKPA_PIECES_TO_START_GAME_SESSION_WITH);
	MainWheelbarrow->DisableSweepMovement();
	MainWheelbarrow->SetActorRotation(ROTATION_OF_WHEELBARROW_WHILE_BEING_PUSHED);
	MainWheelbarrow->StartMovingToSpecificDestination(LOCATION_OF_WHEELBARROW_FINALLY);
	MainCatapult->SetLengthOfProjectilePathOfTrigger(DEFAULT_LENGTH_OF_PROJECTILE_PATH);
	GameSessionIsCurrentlyInProgress = true;
	TimeWhenCurrentGameSessionStarted = GetWorld()->GetTimeSeconds();
	CountOfOkpaPiecesEatenDuringCurrentGameSession = 0;

	if (!GameSessionScreen->IsInViewport()) {
		GameSessionScreen->AddToViewport();
	}

	UpdateGameSessionScreen();
	UWidgetBlueprintLibrary::SetInputMode_GameAndUIEx(GetWorld()->GetFirstPlayerController(), nullptr, EMouseLockMode::DoNotLock, false);
	MainAmbientSound->GetAudioComponent()->SetSound(LoadObject<USoundBase>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("GameSessionMusic"))));
	MainAmbientSound->Play();
}


void AOkpaGameMode::InsertNewOkpaPiecesIntoMainWheelbarrow(int CountOfNewOkpaPiecesToInsert)
{
	AOkpaPiece* OkpaPiece;

	MainWheelbarrow->SetCapacity(MainWheelbarrow->GetNumberOfActorsThatAreBeingHeld() + CountOfNewOkpaPiecesToInsert);

	for (int Count = 1; Count <= CountOfNewOkpaPiecesToInsert; ++Count) {
		OkpaPiece = GetWorld()->SpawnActor<AOkpaPiece>();

		if (OkpaPiece != nullptr) {
			MainWheelbarrow->HoldOkpaPiece(OkpaPiece);
			TrackerOfOkpaPieces->TrackAsInsideWheelbarrow(OkpaPiece);
		}
	}
}


void AOkpaGameMode::CloseCurrentSublevel()
{
	StopGameSession();
	ManagerOfSublevels->CloseCurrentSublevel();
}


void AOkpaGameMode::StopGameSession()
{
	MainWheelbarrow->StopMoving();
	MainCatapult->StopMoving();

	MainWheelbarrow->StopHoldingAllHeldOkpaPieces();
	MainCatapult->CauseTriggerToStopHoldingHeldOkpaPiece();
	MainBowl->StopHoldingAllHeldOkpaPieces();
	TrackerOfOkpaPieces->StopTrackingAllTrackedOkpaPieces();
	MainCatapult->CauseTriggerToBecomeUngriped();
	MainCatapult->DeleteProjectilePathOfTrigger();

	UOkpaLibrary::DestroyAllActorsOfClass(AOkpaPiece::StaticClass(), this);
	UOkpaLibrary::DestroyAllActorsOfClass(AMagnet::StaticClass(), this);
	UOkpaLibrary::GetOkpaHUD(this)->StopDisplayingTutorial();
	UOkpaLibrary::GetOkpaHUD(this)->StopDisplayingCurrentFadeoutTexts();
	UOkpaLibrary::GetOkpaHUD(this)->StopDisplayingCurrentCallouts();

	MainWheelbarrow->SetActorLocation(LOCATION_OF_WHEELBARROW_INITIALLY);
	MainCatapult->SetActorLocation(LOCATION_OF_CATAPULT_INITIALLY);
	MainBowl->SetActorLocation(LOCATION_OF_BOWL);
	MainAmbientSound->Stop();

	GameSessionIsCurrentlyInProgress = false;
	CountOfOkpaPiecesEatenDuringCurrentGameSession = -1;
	TimeSinceOkpaPieceLastLandedInCurrentGameSession = -1;
}


void AOkpaGameMode::RestartGameSession()
{
	StopGameSession();
	StartGameSession();
}


void AOkpaGameMode::PauseGameSession()
{
	if (!UGameplayStatics::IsGamePaused(this)) {
		UGameplayStatics::SetGamePaused(this, true);
		PauseScreen->AddToViewport();
		UWidgetBlueprintLibrary::SetInputMode_UIOnlyEx(GetWorld()->GetFirstPlayerController(), PauseScreen);
	}
}


void AOkpaGameMode::ResumeGameSession()
{
	if (UGameplayStatics::IsGamePaused(this)) {
		PauseScreen->RemoveFromViewport();
		UGameplayStatics::SetGamePaused(this, false);
		UWidgetBlueprintLibrary::SetInputMode_GameAndUIEx(GetWorld()->GetFirstPlayerController(), nullptr, EMouseLockMode::DoNotLock, false);
	}
}


float AOkpaGameMode::GetTimeElapsedSinceCurrentGameSessionStarted() const
{
	if (GameSessionIsCurrentlyInProgress) {
		return GetWorld()->GetTimeSeconds() - TimeWhenCurrentGameSessionStarted;
	}
	else {
		return -1.0f;
	}
}


void AOkpaGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!GameSessionIsCurrentlyInProgress) {
		return;
	}

	if (AtLeastOneOkpaPieceHasLandedInCurrentGameSession) {
		TimeSinceOkpaPieceLastLandedInCurrentGameSession += DeltaTime;
	}

	TArray<AOkpaPiece*> OkpaPiecesThatCouldMissTarget = TrackerOfOkpaPieces->GetOkpaPiecesThatAreLitteredButNotFullyStopped();
	int NumberOfOkpaPiecesThatCouldMissTarget = OkpaPiecesThatCouldMissTarget.Num();

	for (int i = 0; i < NumberOfOkpaPiecesThatCouldMissTarget; ++i) {
		if (OkpaPiecesThatCouldMissTarget[i]->GetVelocity().IsNearlyZero(0.0001f)) {
			TrackerOfOkpaPieces->TrackAsLitteredAndFullyStopped(OkpaPiecesThatCouldMissTarget[i]);
			UOkpaLibrary::GetOkpaHUD(this)->DisplayFadeoutText(TARGET_MISS_TEXT, OkpaPiecesThatCouldMissTarget[i]->GetActorLocation());
			UGameplayStatics::PlaySoundAtLocation(this, LoadObject<USoundBase>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("TargetMissSound"))), LOCATION_OF_OKPA_PIECE_EATER);
		}
	}

	if (TrackerOfOkpaPieces->AllOkpaPiecesHaveLanded() && TimeSinceOkpaPieceLastLandedInCurrentGameSession > TIME_LAG_BEFORE_GAME_SESSION_SHOULD_END) {
		if (CountOfOkpaPiecesEatenDuringCurrentGameSession >= GetMinimumCountOfEatenOkpaPiecesRequiredToWinCurrentGameSession()) {
			SaveGamePlayerAchievementOfCurrentSessionIntoSlot();
			UpdateGameSessionScreen();
			SuccessScreen->AddToViewport();
			MainAmbientSound->GetAudioComponent()->SetSound(LoadObject<USoundBase>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("SuccessMusic"))));
		}
		else {
			FailureScreen->AddToViewport();
			MainAmbientSound->GetAudioComponent()->SetSound(LoadObject<USoundBase>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("FailureMusic"))));
		}

		StopGameSession();
		MainAmbientSound->Play();
		UWidgetBlueprintLibrary::SetInputMode_UIOnlyEx(GetWorld()->GetFirstPlayerController());
	}
}


int AOkpaGameMode::GetMinimumCountOfEatenOkpaPiecesRequiredToWinCurrentGameSession() const
{
	return MainWheelbarrow->GetCapacity() / 2;
}


void AOkpaGameMode::SaveGamePlayerAchievementOfCurrentSessionIntoSlot()
{
	GamePlayerAchievement->SetCountOfOkpaPiecesThatWereEatenInSublevel(ManagerOfSublevels->GetIdentifierOfCurrentSublevel(), CountOfOkpaPiecesEatenDuringCurrentGameSession);
	GamePlayerAchievement->AddToCowriesOwned(GetCowrieRewardForAchievementInCurrentGameSession());
	UGameplayStatics::SaveGameToSlot(GamePlayerAchievement, NAME_OF_SLOT_FOR_SAVING_GAME, INDEX_OF_SLOT_FOR_SAVING_GAME);
}


void AOkpaGameMode::ProcessActorDestinationArrival(AActor* ActorThatArrivedAtDestination, const FVector& Destination)
{
	if (ActorThatArrivedAtDestination == MainWheelbarrow) {
		MainWheelbarrow->SetActorRotation(FRotator::ZeroRotator);
		FVector LocationOfWheelbarrowRider(Destination.X + 50, Destination.Y, Destination.Z + 150);
		UOkpaLibrary::GetOkpaHUD(this)->DisplayCallout(WHEELBARROW_ARRIVAL_TEXT, LocationOfWheelbarrowRider);
		UGameplayStatics::PlaySoundAtLocation(this, LoadObject<USoundBase>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("WheelbarrowArrivalSound"))), LocationOfWheelbarrowRider);
		MainCatapult->DisableSweepMovement();
		MainCatapult->StartMovingToSpecificDestination(LOCATION_OF_CATAPULT_FINALLY);
	}
	else if (ActorThatArrivedAtDestination == MainCatapult) {
		TransferOneOkpaPieceFromMainWheelbarrowToMainCatapultTrigger();
	}
	else if (ManagerOfSublevels->ActorIsMovableObstacle(ActorThatArrivedAtDestination)) {
		ManagerOfSublevels->ReverseDirectionOfMovableObstacle(ActorThatArrivedAtDestination);
	}
	else if (Cast<AOkpaPiece>(ActorThatArrivedAtDestination) != nullptr && Destination.Equals(MainCatapult->GetBaseLocationOfTrigger())) {
		AOkpaPiece* OkpaPieceThatArrivedAtDestination = Cast<AOkpaPiece>(ActorThatArrivedAtDestination);
		MainCatapult->CauseTriggerToHoldOkpaPiece(OkpaPieceThatArrivedAtDestination);
		TrackerOfOkpaPieces->TrackAsInsideCatapultTrigger(OkpaPieceThatArrivedAtDestination);

		if (ManagerOfSublevels->GetIdentifierOfCurrentSublevel() == FIRST_SUBLEVEL && OnlyOneOkpaPieceHasMovedFromMainWheelbarrowToMainCatapultTrigger) {
			UOkpaLibrary::GetOkpaHUD(this)->DisplayTutorial();
		}
	}
}


AOkpaPiece* AOkpaGameMode::TransferOneOkpaPieceFromMainWheelbarrowToMainCatapultTrigger()
{
	AOkpaPiece* OkpaPieceThatIsToBeTransferred = MainWheelbarrow->RemoveOneOkpaPiece();

	if (OkpaPieceThatIsToBeTransferred != nullptr) {
		OkpaPieceThatIsToBeTransferred->DisableSweepMovement();
		OkpaPieceThatIsToBeTransferred->StartMovingToSpecificDestination(MainCatapult->GetBaseLocationOfTrigger(), 0.8);
	}

	return OkpaPieceThatIsToBeTransferred;
}


void AOkpaGameMode::ProcessActorHit(AActor* ActorThatHitAnotherActor, AActor* ActorThatWasHit, UPrimitiveComponent* ComponentThatHitAnotherComponent, UPrimitiveComponent* ComponentThatWasHit)
{
	AOkpaPiece* OkpaPiece = Cast<AOkpaPiece>(ActorThatHitAnotherActor);

	if (OkpaPiece != nullptr && !TrackerOfOkpaPieces->OkpaPieceIsInsideBowl(OkpaPiece)) {
		if (ComponentIsEnteranceOfMainBowl(ComponentThatWasHit)) {
			TrackerOfOkpaPieces->TrackAsInsideBowl(OkpaPiece);
			TimeSinceOkpaPieceLastLandedInCurrentGameSession = 0;
			++CountOfOkpaPiecesEatenDuringCurrentGameSession;
			UpdateGameSessionScreen();
			UOkpaLibrary::GetOkpaHUD(this)->DisplayFadeoutText(TARGET_HIT_TEXT, OkpaPiece->GetActorLocation());
			UGameplayStatics::PlaySoundAtLocation(this, LoadObject<USoundBase>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("TargetHitSound"))), OkpaPiece->GetActorLocation());
			MainCatapult->DeleteProjectilePathOfTrigger();
		}
		else if (!TrackerOfOkpaPieces->OkpaPieceIsLittered(OkpaPiece)) {
			TrackerOfOkpaPieces->TrackAsLittered(OkpaPiece);
			TimeSinceOkpaPieceLastLandedInCurrentGameSession = 0;
			MainCatapult->DeleteProjectilePathOfTrigger();
		}
	}
}


void AOkpaGameMode::ProcessOkpaPieceLaunch(AOkpaPiece* OkpaPieceThatWasLaunched)
{
	TrackerOfOkpaPieces->TrackAsLaunched(OkpaPieceThatWasLaunched);
	UpdateGameSessionScreen();

	if (!MainWheelbarrow->IsEmpty()) {
		TransferOneOkpaPieceFromMainWheelbarrowToMainCatapultTrigger();
	}
}


void AOkpaGameMode::UpdateGameSessionScreen() const
{
	GameSessionScreen->OnAnalogValueChanged(FGeometry(), FAnalogInputEvent());
}


void AOkpaGameMode::ProcessOkpaPieceEat(AOkpaPiece* OkpaPieceThatWasEaten)
{
	int RandomIndex;

	TrackerOfOkpaPieces->StopTrackingFromInsideBowl(OkpaPieceThatWasEaten);
	OkpaPieceThatWasEaten->Destroy();

	RandomIndex = FMath::Rand() % TextsThatEmphasizeDeliciousnessOfOkpa.Num();
	UGameplayStatics::PlaySoundAtLocation(this, LoadObject<USoundBase>(nullptr, *UOkpaLibrary::GetReferencePathOfAsset(TEXT("PopSound"))), LOCATION_OF_OKPA_PIECE_EATER);
	UOkpaLibrary::GetOkpaHUD(this)->DisplayCallout(TextsThatEmphasizeDeliciousnessOfOkpa[RandomIndex], LOCATION_OF_OKPA_PIECE_EATER);
}


void AOkpaGameMode::ProcessPaperFlipbookAnimationFinish()
{
	int Index = 0;
	TArray<AActor*> SpawnedPaperFlipbookActors;

	UGameplayStatics::GetAllActorsOfClass(this, APaperFlipbookActor::StaticClass(), SpawnedPaperFlipbookActors);

	while (Index < SpawnedPaperFlipbookActors.Num()) {
		if (Cast<APaperFlipbookActor>(SpawnedPaperFlipbookActors[Index])->GetRenderComponent()->IsPlaying()) {
			++Index;
		}
		else {
			SpawnedPaperFlipbookActors[Index]->Destroy();
			SpawnedPaperFlipbookActors.RemoveAt(Index);
			Index = 0;
		}
	}
}


bool AOkpaGameMode::OkpaPieceHasNeverBeenLaunched(AOkpaPiece* OkpaPiece) const
{
	return TrackerOfOkpaPieces->OkpaPieceIsInsideWheelbarrow(OkpaPiece) || TrackerOfOkpaPieces->OkpaPieceIsInsideCatapultTrigger(OkpaPiece);
}

bool AOkpaGameMode::OkpaPieceIsInsideBowl(AOkpaPiece * OkpaPiece) const
{
	return TrackerOfOkpaPieces->OkpaPieceIsInsideBowl(OkpaPiece);
}


int AOkpaGameMode::GetCountOfOkpaPiecesThatAreYetToBeLaunched() const
{
	return MainWheelbarrow->GetNumberOfActorsThatAreBeingHeld() + MainCatapult->TriggerIsNotEmpty();
}


bool AOkpaGameMode::ComponentIsEnteranceOfMainBowl(const UPrimitiveComponent* Component) const
{
	return MainBowl->ComponentIsEnteranceOfThisBowl(Component);
}


int AOkpaGameMode::GetIdentifierOfHighestSublevelThatHasEverBeenCompleted() const
{
	return GamePlayerAchievement->GetIdentifierOfHighestSublevelThatHasEverBeenCompleted(); 
}


int AOkpaGameMode::GetHighestCountOfOkpaPiecesThatHaveEverBeenEatenInSublevel(int Sublevel) const
{
	return GamePlayerAchievement->GetHighestCountOfOkpaPiecesThatHaveEverBeenEatenInSublevel(Sublevel);
}


int AOkpaGameMode::GetIdentifierOfCurrentSublevel() const
{
	return ManagerOfSublevels->GetIdentifierOfCurrentSublevel();
}


int AOkpaGameMode::GetCountOfOkpaPiecesEatenDuringCurrentGameSession() const
{
	return CountOfOkpaPiecesEatenDuringCurrentGameSession;
}


int AOkpaGameMode::GetCowrieRewardForAchievementInCurrentGameSession() const
{
	return CountOfOkpaPiecesEatenDuringCurrentGameSession * COWRIE_REWARD_PER_EATEN_OKPA_PIECE;
}


int AOkpaGameMode::GetAmountOfCowriesOwnedByGamePlayer() const
{
	return GamePlayerAchievement->GetAmountOfCowriesOwned();
}


TArray<FDataAboutOkpaGameBooster> AOkpaGameMode::GetDataAboutBoostersOwnedByGamePlayer() const
{
	TArray<FDataAboutOkpaGameBooster> DataAboutBoostersOwned;
	const TArray<FName>& NamesOfBoostersOwned = GamePlayerAchievement->GetNamesOfBoostersOwned();

	DataAboutBoostersOwned.AddDefaulted(NamesOfBoostersOwned.Num());

	for (int i = 0; i < NamesOfBoostersOwned.Num(); ++i) {
		DataAboutBoostersOwned[i] = UOkpaLibrary::GetDataAboutBooster(NamesOfBoostersOwned[i]);
	}

	return DataAboutBoostersOwned;
}


int AOkpaGameMode::GetQuantityOfBoosterOwnedByGamePlayer(const FName& NameOfBooster) const
{
	return GamePlayerAchievement->GetQuantityOwnedOfBooster(NameOfBooster);
}


TArray<FDataAboutOkpaGameBooster> AOkpaGameMode::GetDataAboutBoostersSoldInMarketplace() const
{
	return UOkpaLibrary::GetDataAboutBoostersSoldInMarketplace();
}


FDataAboutOkpaGameBooster AOkpaGameMode::GetDataAboutBooster(const FName& NameOfBooster) const
{
	return UOkpaLibrary::GetDataAboutBooster(NameOfBooster);
}


int AOkpaGameMode::GetCapacityOfMainWheelbarrow() const
{
	return MainWheelbarrow->GetCapacity();
}


FVector AOkpaGameMode::GetBaseLocationOfMainCatapultTrigger() const
{
	return MainCatapult->GetBaseLocationOfTrigger();
}
// Catapult.h: Catapult class definition.

#pragma once

#include "CoreMinimal.h"
#include "Classes/PaperSpriteComponent.h"
#include "ActorThatCanMove.h"
#include "OkpaPiece.h"
#include "Catapult.generated.h"


/**
 * Catapult defines an actor that can own a trigger and used to launch okpa pieces.
 * @author Ejiofor,  Ifechukwu.
 */
UCLASS()
class OKPA_API ACatapult : public AActorThatCanMove
{
	GENERATED_BODY()
	
public:
	/** Sets default values for this catapult's propereties. */
	ACatapult();

	/** Deactivates all existing catapults in the specified world. */
	static void DeactivateAllExistingCatapults(const UObject* WorldContextObject);

	/** Activates this catapult.
		@note Only an activated catapult can launch okpa pieces. */
	void Activate();

	/** Deactivates this catapult.
		@see Activate. */
	void Deactivate();

	/** Returns true if this catapult is activated. Returns false otherwise. */
	bool IsActivated() const;

	/** Returns the base location of this catapult's trigger. */
	FVector GetBaseLocationOfTrigger() const;

	/** Returns the current location of this catapult's trigger. */
	FVector GetCurrentLocationOfTrigger() const;

	/** Deletes the visible projectile path of this catapult's trigger). */
	void DeleteProjectilePathOfTrigger();

	/** Sets the length of the projectile path of this catapult's trigger. */
	void SetLengthOfProjectilePathOfTrigger(float Length);

	/** Returns true if this catpult is activated and its trigger is holding an okpa piece. Returns false otherwise. */
	bool TriggerIsNotEmpty() const;

	/** Returns true if this catapult is activated and its trigger is holding the specified okpa piece. Returns false otherwise. */
	bool TriggerIsHoldingOkpaPiece(const AOkpaPiece* OkpaPiece) const;

	/** Causes this catapult's trigger to become ungriped. */
	void CauseTriggerToBecomeUngriped();

	/** Causes this catapult's trigger to hold the specified okpa piece. */
	void CauseTriggerToHoldOkpaPiece(AOkpaPiece* OkpaPiece);

	/** Causes this catapult's trigger to stop holding its held okpa piece (if this catapult is holding an okpa piece). */
	void CauseTriggerToStopHoldingHeldOkpaPiece();

	/** Moves this catapult, and its trigger, instantly to a new location.
		@see AActor::SetActorLocation. */
	bool SetActorLocation(const FVector& NewLocation, bool bSweep = false, FHitResult* OutSweepHitResult = nullptr, ETeleportType Teleport = ETeleportType::None);

	/** Returns the component that is used to render this catapult. */
	USceneComponent* GetRenderComponent() const;

	/** Performs the processing that is to be performed periodically. */
	virtual void Tick(float DeltaTime) override;

protected:
	/** Performs the processing that is to be performed when this catapult is spawned. */
	virtual void BeginPlay() override;

	/** Component that is used to render this catapult. */
	UPROPERTY(VisibleAnywhere)
	UPaperSpriteComponent* RenderComponent;

	/** Trigger of this catapult. */
	UPROPERTY()
	class ACatapultTrigger* Trigger;
};
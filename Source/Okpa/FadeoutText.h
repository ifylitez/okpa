// FadeoutText.h: FadeoutText class definition.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TextRenderActor.h"
#include "FadeoutText.generated.h"

/**
 * FadeoutText defines a TextRenderActor that starts fading out as soon as it is spawned.
 * @author EjioforIfechukwu.
 */
UCLASS()
class OKPA_API AFadeoutText : public ATextRenderActor
{
	GENERATED_BODY()
	
public:
	/** Sets default value for this fadeout text's properties. */
	AFadeoutText();

	/** Performs processing that is to be performed periodically. */
	virtual void Tick(float DeltaTime) override;

	/** Sets the lifespan of this fadeout text.
		@see AActor::SetLifeSpan. */
	virtual void SetLifeSpan(float NewLifespan) override;

	/** Sets the speed at which this fadeout text scales. */
	void SetScaleSpeed(float NewScaleSpeed);

protected:
	/** Performs processing that is to be performed when this fadeout text is spawned. */
	virtual void BeginPlay() override;

	/** Re-calculates the late scale speed of this fadeout text.
		@see LateScaleSpeed. */
	void ReCalculateLateScaleSpeed();

protected:
	/** The number of seconds that have elapsed since this fadeout text was spawned. */
	float Age;

	/** Speed at which this fadeout text scales at the beginning of its lifetime. */
	float EarlyScaleSpeed;

	/** Speed at which this fadeout text scales towards the end of its lifetime. */
	float LateScaleSpeed;
};